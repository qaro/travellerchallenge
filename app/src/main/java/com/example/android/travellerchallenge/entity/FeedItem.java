package com.example.android.travellerchallenge.entity;

/**
 * This is the Feed Class.
 * Feed is basically the news about followed users
 */

public class FeedItem {

    private String mUserName;

    private String mFeedsContent;

    private String mProfileImageID;

    private long mTimeInMilliseconds;

    public FeedItem ( String userName,String feedsContent, String photoUri, long timeInMilliseconds ){
        mUserName = userName;
        mFeedsContent = feedsContent;
        mProfileImageID = photoUri;
        mTimeInMilliseconds = timeInMilliseconds;
    }

    public String getUserName(){
        return mUserName;
    }
    public String getFeedsContent(){
        return mFeedsContent;
    }
    public String getProfileImageID() { return mProfileImageID;}
    public long getTimeInMilliseconds() {return mTimeInMilliseconds;}
}
