package com.example.android.travellerchallenge.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.android.travellerchallenge.R;
import com.example.android.travellerchallenge.constant.ProjectConstants;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import com.example.android.travellerchallenge.adapter.UserAdapter;
import com.example.android.travellerchallenge.entity.User;

/**
 * This is the activity to display followers and following users of the current user.
 * The userID data will be retrieved from the "followerData" and  "followingData" child of the database.
 * Then from the "users" child, the information of user will be retrieved by these ID's and will be displayed.
 */

public class FollowInfoActivity extends AppCompatActivity {
    private Activity context;

    private static final String TAG = "FollowInfoActivity";

    private ArrayList<String> userIdList = new ArrayList<>();
    private ArrayList<User> userList = new ArrayList<>();
    private ListView listView;
    private String userID;
    private String pageName;
    private UserAdapter userAdapter;

    private DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feed_list);
        context = this;

        listView = findViewById(R.id.feed_list);

        // getting type (followers, following) and userId from bundle
        Bundle b = getIntent().getExtras();
        if (b != null){
            pageName = b.getString("type");
            userID = b.getString("id");
            Log.v(TAG, "Created activity for " + userID);
        }
        // setting title according to type
        setTitle(pageName);

        // display users according to type of the activity
        if (pageName.equals(ProjectConstants.FOLLOWERS)) {
            listFollowers(userID);
        } else if (pageName.equals(ProjectConstants.FOLLOWING)){
            listFollowings(userID);
        }

        // setting onclick listener to list items
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(context,UserProfileActivity.class);
                Bundle b = new Bundle();
                User user = userAdapter.getItem(position);
                if(user != null)
                    b.putString("userid", user.getUserID());
                intent.putExtras(b);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * method to call to list the Followers of the current user
      */

    public void listFollowers(String userId){

        databaseReference = FirebaseDatabase.getInstance().getReference();

        databaseReference.child(ProjectConstants.FOLLOWERS_ENTITY).child(userId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    // add all followers' userIDs of the current users to list
                    for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                        userIdList.add(snapshot.getKey());
                    }
                    // then from "users" child, get user info of all followers by userIDList
                    databaseReference.child(ProjectConstants.USERS_ENTITY).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()){
                                for(String userId : userIdList){
                                    //get user and add it to user list
                                    userList.add(dataSnapshot.child(userId).getValue(User.class));
                                    userAdapter = new UserAdapter(context, userList);
                                    listView.setAdapter(userAdapter);
                                }
                            }
                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) { }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    /**
     * method to call to list the Following users of the current user
      */

    public void listFollowings(String userId){
        databaseReference = FirebaseDatabase.getInstance().getReference();

        databaseReference.child(ProjectConstants.FOLLOWING_ENTITY).child(userId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    // add all following users' userIDs of the current users to list
                    for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                        userIdList.add(snapshot.getKey());
                    }
                    // then from "users" child, get user info of all following users by userIDList
                    databaseReference.child(ProjectConstants.USERS_ENTITY).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()){
                                for(String userId : userIdList){
                                    //get user and add it to user list
                                    userList.add(dataSnapshot.child(userId).getValue(User.class));
                                    userAdapter = new UserAdapter(context, userList);
                                    listView.setAdapter(userAdapter);
                                }
                            }

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

    }
}