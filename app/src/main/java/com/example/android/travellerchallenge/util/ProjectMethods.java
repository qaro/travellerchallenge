package com.example.android.travellerchallenge.util;

import android.util.Log;

import com.example.android.travellerchallenge.constant.ProjectConstants;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * This is the class that contains static common methods of the project
 */
public final class ProjectMethods {

    /**
     * method to convert millis in "dd/MM/yy" format
      */
    public static String convertMillisToDate(long timeInMillis){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeInMillis);

        String dateFormat = "dd/MM/yy";
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.US);

        return sdf.format(calendar.getTime());

    }

    /**
     * method to convert the integer duration in minutes to string format such "d h m"
      */
    public static String convertDurationToString(int duration){
        String d = (duration / 1440 != 0)?String.valueOf(duration / 1440) + "d": "";
        String h = ((duration%1440)/60 != 0)?String.valueOf((duration%1440)/60)+"h":"";
        String m = (((duration%1440)%60) != 0)?String.valueOf((duration%1440)%60)+"m":"";
        return d+h+m;
    }

    /**
     * method to convert list of integer that contains place types to string with type names.
      */
    public static String convertTypesToString (List<Integer> categories){
        StringBuilder sb = new StringBuilder(1024);

        for (Integer [] j : ProjectConstants.CATEGORIES){
            for (Integer i : categories){
                if(Arrays.asList(j).contains(i)){
                    int index = Arrays.asList(ProjectConstants.CATEGORIES).indexOf(j);
                    sb.append(ProjectConstants.CATEGORY_NAMES[index]);
                    sb.append(" ");
                    break;
                }
            }

        }

        return sb.toString();
    }

    /**
     * method to convert types to integer values
     */
    public static Integer[] convertTypesToInt (ArrayList<String> categories) {
        Integer [] res = {} ;
        for (String i : categories){
            int index = Arrays.asList(ProjectConstants.CATEGORY_NAMES).indexOf(i);
            Integer[] array = ProjectConstants.CATEGORIES[index];
            res = joinArrays(array,res);
        }
        for (Integer i : res){
            Log.v("ARRAY", String.valueOf(i));
        }
        return res;
    }

    /**
     * method to join two arrays
     */
    private static Integer[] joinArrays(Integer[] newArray, Integer[] oldArray){
        Integer[] result = new Integer[newArray.length + oldArray.length];
        System.arraycopy(newArray, 0, result, 0, newArray.length);
        System.arraycopy(oldArray, 0, result, newArray.length, oldArray.length);
        return result;
    }

}
