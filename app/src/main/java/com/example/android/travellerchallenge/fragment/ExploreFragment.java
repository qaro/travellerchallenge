package com.example.android.travellerchallenge.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.android.travellerchallenge.activity.UserProfileActivity;
import com.example.android.travellerchallenge.adapter.UserAdapter;
import com.example.android.travellerchallenge.constant.ProjectConstants;
import com.example.android.travellerchallenge.R;
import com.example.android.travellerchallenge.entity.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;

/**
 * fragment to display the leaderboard
 */
public class ExploreFragment extends Fragment {
    public ExploreFragment() {
        // Required empty public constructor
    }

    private ArrayList<User> users;
    private UserAdapter userAdapter;

    private final DatabaseReference usersDatabaseRef = FirebaseDatabase.getInstance().getReference().child(ProjectConstants.USERS_ENTITY);

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.explore_layout, container, false);
        setHasOptionsMenu(true);

        ListView listView = rootView.findViewById(R.id.listview);

        users = new ArrayList<>();

        // get users from database
        usersDatabaseRef.orderByChild(ProjectConstants.USERS_SCORE).limitToLast(10).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                userAdapter.clear();
                for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                    userAdapter.add(snapshot.getValue(User.class));
                }
                // desc order
                Collections.reverse(users);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        userAdapter = new UserAdapter(getActivity(), users);
        listView.setAdapter(userAdapter);

        // set clicklistener to user items
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(),UserProfileActivity.class);
                Bundle b = new Bundle();
                User user = userAdapter.getItem(position);
                if(user != null)
                    b.putString("userid", user.getUserID());
                intent.putExtras(b);
                startActivity(intent);
            }
        });

        return rootView;
    }
}
