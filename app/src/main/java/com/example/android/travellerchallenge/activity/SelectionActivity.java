package com.example.android.travellerchallenge.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;


import android.view.MenuItem;
import android.widget.Button;
import android.view.View;

import com.example.android.travellerchallenge.R;
import com.example.android.travellerchallenge.constant.ProjectConstants;
import com.example.android.travellerchallenge.util.ProjectMethods;
import com.google.android.gms.location.places.Place;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Locale;

import com.example.android.travellerchallenge.adapter.SelectionPageAdapter;
import com.example.android.travellerchallenge.fragment.StepOneFragment;
import com.example.android.travellerchallenge.fragment.StepThreeFragment;
import com.example.android.travellerchallenge.fragment.StepTwoFragment;
import com.google.android.gms.maps.model.LatLng;

/**
 * This activity is activity which contains step fragments of challenge selection process such as StepOne and StepTwo
 * This activity is used to do background job of selection process, such as building URI to query the PLACES API .
 * the preferences retrieved from Step 1 and 2 will be stored in this activity and they will be passed to Step3
 */

public class SelectionActivity extends AppCompatActivity implements StepTwoFragment.PassCategory, StepOneFragment.PassPlaceDate{

    private Button nextButton, prevButton;
    private ViewPager viewPager;
    private SelectionPageAdapter adapter;

    private Activity context;

    // Categories from StepTwoFragment
    private ArrayList<String> categories;

    // Start-end place and date from the StepOneFragment
    Calendar startDate = null, endDate = null;
    LatLng startPlaceLatLng = null, endPlaceLatLng = null;
    String endName = null, startName = null, startTimeText = null, endTimeText= null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(ProjectConstants.CHALLENGE_ME);
        context=this;
        setContentView(R.layout.selection_layout);

        // Find the view pager that will allow the user to swipe between com.example.android.travellerchallenge.fragments
        viewPager = findViewById(R.id.viewpager);

        // Create an adapter that knows which fragment should be shown on each page
        adapter = new SelectionPageAdapter(this, getSupportFragmentManager());

        // Set the adapter onto the view pager
        viewPager.setAdapter(adapter);

        // Set page listener to view pager
        viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                // When tab change update bottom bar
                updateBottomBar();

                // If last step fragment has been selected, send place, date and category information
                if(position == 2){
                    String tag = "android:switcher:" + R.id.viewpager + ":" + 2;
                    StepThreeFragment f = (StepThreeFragment) getSupportFragmentManager().findFragmentByTag(tag);
                    f.updateCategory(categories);
                    f.updatePlace(startName,endName);
                    f.updateDate(startTimeText,endTimeText);
                }

            }
        });

        nextButton = findViewById(R.id.next_button);
        prevButton = findViewById(R.id.prev_button);

        // Set click listener to next button
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (viewPager.getCurrentItem() == adapter.getCount() -1 ) {

                    ArrayList<Integer> typeList = new ArrayList<>(Arrays.asList(ProjectMethods.convertTypesToInt(categories)));

                    // start search result activity with the start-end point, uri and type parameters
                    Intent intent = new Intent(context, SearchResultActivity.class);
                    Bundle b = new Bundle();
                    b.putString("uri", buildURI());
                    b.putIntegerArrayList("types",typeList);
                    b.putParcelable("start_point", startPlaceLatLng);
                    b.putParcelable("end_point", endPlaceLatLng);
                    intent.putExtras(b);
                    startActivity(intent);
                } else {
                        // Step next fragment on viewpager
                        viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                }
            }
        });

        // Set click listener to next button
        prevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Step previous fragment on viewpager
                viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
            }
        });

        updateBottomBar();
    }

    /**
     * Method to update bottom bar according to displayed fragment's position
     */
    private void updateBottomBar() {
        int position = viewPager.getCurrentItem();
        // For last fragment, the next button will be displayed as Search button
        // For first fragment, the previous button will be invisible
        if (position == adapter.getCount() - 1) {
            nextButton.setText(ProjectConstants.SEARCH);
        } else {
            nextButton.setText(ProjectConstants.NEXT);
        }
        prevButton.setVisibility(position <= 0 ? View.INVISIBLE : View.VISIBLE);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // The override method of PassCategory interface
    @Override
    public void passCategoryData(ArrayList<String> categories) {
        this.categories = categories;
    }

    // The override method of PassPlaceDate interface
    @Override
    public void passDateData(Calendar start, Calendar end){
        startDate = start;
        endDate = end;

        String dateFormat = "dd/MM/yy " + "HH:mm";
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.US);

        startTimeText = sdf.format(start.getTime());
        endTimeText = sdf.format(end.getTime());
    }

    // The override method of PassPlaceDate interface
    @Override
    public void passPlaceData(Place start, Place end){
        if(end != null) {
            endPlaceLatLng = end.getLatLng();
            endName = String.valueOf(end.getName());
        } else {
            endName = "Not Selected";
        }
        if(start != null) {
            startPlaceLatLng = start.getLatLng();
            startName = String.valueOf(start.getName());
        }
    }

    /**
     * method to Build URI to query PLACES API with given preferences from steps
      */
    private String buildURI(){
        // TODO : build URI with user entries
        return "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=-33.8670522,151.1957362&radius=1500&type=restaurant&keyword=cruise&key=AIzaSyDgbYmUVgqMosBdNSLjdt4HcFZwISQS1hU";

    }

}