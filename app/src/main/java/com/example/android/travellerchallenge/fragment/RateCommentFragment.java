package com.example.android.travellerchallenge.fragment;

import android.app.Activity;
import android.app.DialogFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.travellerchallenge.R;
import com.example.android.travellerchallenge.constant.ProjectConstants;
import com.example.android.travellerchallenge.entity.ReviewItem;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;



public class RateCommentFragment extends DialogFragment {

    private static final String TAG = "RateCommentFragment";

    private Activity context;
    private RatingBar ratingBar;
    private EditText commentContentEditText;

    private String challengeId;
    private double challengeCurrentRate;
    private int challengeCurrentRateCount;
    private double rate;

    private static FirebaseUser currentUser;
    private static final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.rate_dialog_layout, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        currentUser = FirebaseAuth.getInstance().getCurrentUser();

        context = getActivity();

        challengeId = getArguments().getString("challengeID");

        ratingBar = rootView.findViewById(R.id.ratingBarId);

        commentContentEditText = rootView.findViewById(R.id.comment);
        TextView submitButton = rootView.findViewById(R.id.submit);
        TextView cancelButton = rootView.findViewById(R.id.cancel);

        databaseReference.child(ProjectConstants.CHALLENGES_ENTITY).child(challengeId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    challengeCurrentRate = dataSnapshot.child(ProjectConstants.CHALLENGES_RATE).getValue(double.class);
                    challengeCurrentRateCount = dataSnapshot.child(ProjectConstants.CHALLENGES_RATECOUNT).getValue(int.class);
                }
                else{
                    Log.i(TAG, "onDataChange: Error getting challenge current rate with challengeId: " + challengeId);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        // submit button
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appendFirebaseWithReview();
                getActivity().finish();
            }
        });

        // cancel button
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();

        Window dialogWindow = getDialog().getWindow();
        if (dialogWindow != null) {
            dialogWindow.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    /**
     * method to uppend new review to the database
     */
    public void appendFirebaseWithReview(){

        ReviewItem reviewItem = new ReviewItem();

        rate = ratingBar.getRating();
        String commentContent = commentContentEditText.getText().toString();

        DatabaseReference reviewsReference = databaseReference.child(ProjectConstants.REVIEWS_ENTITY).child(challengeId);
        String reviewId = reviewsReference.push().getKey();

        reviewItem.setCommentContent(commentContent);
        reviewItem.setRate(rate);
        reviewItem.setReviewID(reviewId);
        reviewItem.setTimestamp(-System.currentTimeMillis());
        reviewItem.setUserID(currentUser.getUid());

        reviewsReference.child(reviewId).setValue(reviewItem);

        Toast.makeText(context, "Review submitted.", Toast.LENGTH_SHORT).show();
        Log.i(TAG, "appendFirebaseWithReview: Review submitted by user with userId: "
                + currentUser.getUid()
                + " on challenge with challengeId: "
                + challengeId
                + ".");
        
        updateChallengesOnFirebase();
    }

    /**
     * method to update challenge with the new review content such as rate and comment
     */
    public void updateChallengesOnFirebase(){

        int newRatingCount = challengeCurrentRateCount + 1;
        double newRating = ((challengeCurrentRate * challengeCurrentRateCount) + rate) / newRatingCount;

        DatabaseReference challengesReference = databaseReference.child(ProjectConstants.CHALLENGES_ENTITY);

        challengesReference.child(challengeId).child(ProjectConstants.CHALLENGES_RATECOUNT).setValue(newRatingCount);
        challengesReference.child(challengeId).child(ProjectConstants.CHALLENGES_RATE).setValue(newRating);

        Log.i(TAG, "updateChallengesOnFirebase: Challenge rating and ratingCount are updated.");

    }

}
