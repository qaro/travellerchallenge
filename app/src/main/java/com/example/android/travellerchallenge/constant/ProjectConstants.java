package com.example.android.travellerchallenge.constant;


/**
 * This class contains the system constants. They can be reached as static variables.
**/

public final class ProjectConstants {
    // Activity Status
    public static final int NOTVISITED = 1;
    public static final int ONGOING = 2;
    public static final int COMPLETED = 3;

    // Challenge status
    public static final String CHALLENGE_CANCELED = "CANCELED";
    public static final String CHALLENGE_FAILED = "FAILED";
    public static final String CHALLENGE_SUCCESFULL = "SUCCESSFUL";

    // Constant value for checking distance to activity POI
    public static final float DISTANCE_MAX = 30;

    // Titles and names
    public static final String EDIT_PROFILE = "Edit Profile";
    public static final String CHALLENGE_ME = "Challenge Me";
    public static final String HAS_ONGOING_CHALLENGE_TITLE = "You are already on duty!";
    public static final String HAS_ONGOING_CHALLENGE_MESSAGE = "You can not start another challenge right now!";
    public static final String CREATE_CHALLENGE = "Create Challenge";
    public static final String SAVE_CHANGES = "Save Changes";
    public static final String COUNTDOWN_OVER_TITLE = "Game over!";
    public static final String FOLLOWERS = "Followers";
    public static final String FOLLOWING = "Following";
    public static final String RATE_COMMENT = "Rate/Review";
    public static final String CHALLENGE_SUCCESFULLY_DONE_TITLE = "Congratulations!";
    public static final String ON_DUTY = "You Are On Duty!";

    // Home feed messages
    public static final String USER_STARTED_CHALLENGE_MESSAGE = "Started challenge named: ";

    public static final String USER_COMPLETED_CHALLENGE_MESSAGE = "Completed challenge named: ";
    public static final String USER_GAINED_SCORE_MESSAGE = " and gained ";
    public static final String SCORE_MESSAGE = " points.";

    public static final String USER_CANCELLED_CHALLENGE_MESSAGE = "Cancelled challenge named: ";
    public static final String USER_FAILED_CHALLENGE_MESSAGE = "Failed to complete challenge named: ";

    // Alert Dialog Messages
    public static final String START_CHALLENGE_MESSAGE = "Are you sure to play this challenge ?";
    public static final String CREATE_CHALLENGE_MESSAGE ="Are you sure to create this challenge ?";
    public static final String SAVE_CHANGES_MESSAGE ="Are you sure to save changes ?";
    public static final String CANCEL_CHALLENGE_MESSAGE="Your ongoing challenge will be canceled. Are you sure to proceed?";
    public static final String COUNTDOWN_OVER_MESSAGE = "Your time is over, try again!";
    public static final String CHALLENGE_SUCCESFULLY_DONE_MESSAGE = "You have succesfully completed this challenge!";

    public static final String POSITIVE_ANSWER = "Yes";
    public static final String NEGATIVE_ANSWER = "No";
    public static final String DONE = "Done";
    public static final String SEARCH = "Search";
    public static final String NEXT = "Next";
    public static final String NEUTRAL_ANSWER = "OK";


    // User Information Messages
    public static final String PASSWORD_CHANGE_SUCCESSFUL ="Password has been changed";
    public static final String PASSWORD_CHANGE_FAILED = "Error password not updated.";
    public static final String CURRENT_PASSWORD_ERROR = "Current password is not correct.";
    public static final String IMAGE_UPLOAD_SUCCESSFUL ="Image upload is succeed.";
    public static final String IMAGE_UPLOAD_FAILED = "Image upload is failed.";
    public static final String CHALLENGE_CREATED = "Challenge have been saved.";
    public static final String CHANGES_SAVED ="Changes have been saved.";
    public static final String GOOGLE_PLAY_SERVICES_NOT_AVAILABLE = "Google Play Services is not available.";
    public static final String LOGIN_SUCCESSFUL ="Login Success";
    public static final String LOGIN_FAILED = "Login Failed";
    public static final String SIGNUP_SUCCESSFUL ="Sign Up Success";
    public static final String SIGNUP_FAILED = "Sign Up Failed";
    public static final String FOLLOW_SUCCESSFUL ="Followed successfully";
    public static final String UNFOLLOW_SUCCESSFUL ="Unfollowed user";

    // Input Validation Messages
    public static final String CHALLENGE_NAME_REQUIRED ="Please enter challenge name";
    public static final String CHALLENGE_ACTIVITY_REQUIRED = "At least one activity is required";
    public static final String CHALLENGE_DURATION_REQUIRED= "Please enter a valid duration";
    public static final String CONFIRMATION_REQUIRED ="Please confirm password";
    public static final String PASSWORD_SIZE_REQUIRED= "Password must be larger than 6 characters";
    public static final String CURRENT_PASSWORD_REQUIRED = "Current password must be entered";
    public static final String USERNAME_SIZE_REQUIRED = "At least 3 characters are required";
    public static final String EMAIL_REQUIRED = "Please enter a valid e-mail address";
    public static final String PASSWORD_REQUIRED= "Please enter valid password";

    // Menu Items
    public static final int MENU_ITEM_CREATE = 0;
    public static final int MENU_ITEM_SAVE = 1;
    public static final int MENU_ITEM_CREATE_CHALLENGE = 2;

    // Place Pickers
    public static final int ORIGIN_PLACE_PICKER_REQUEST = 1;
    public static final int DESTINATION_PLACE_PICKER_REQUEST = 2;

    // Request and Result ProjectConstants
    public static final int PLACE_PICKER_REQUEST = 1;
    public static final int RESULT_LOAD_IMAGE = 2;

    public static final int REQUEST_SIGNUP = 0;
    public static final int RC_SIGN_IN = 2;

    public static final int PLACE_LOADER_ID = 1;

    public static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;

    // Firebase Database Entities ProjectConstants
    public static final String CHALLENGES_ENTITY = "challenges";
    public static final String CHALLENGES_CNAME = "cname";
    public static final String CHALLENGES_CITY = "city";
    public static final String CHALLENGES_RATE = "rate";
    public static final String CHALLENGES_RATECOUNT= "rateCount";
    public static final String CHALLENGES_SCORE = "score";
    public static final String CHALLENGES_DATE = "date";
    public static final String CHALLENGES_DURATION= "duration";
    public static final String CHALLENGES_CREATORID = "creatorID";
    public static final String CHALLENGES_ACTIVITIES = "activities";
    public static final String CHALLENGES_TYPES= "types";

    public static final String HOME_FEED_ENTITY = "feedContent";

    public static final String USERS_ENTITY = "users";
    public static final String USERS_USERNAME = "username";
    public static final String USERS_CHALLENGES_CREATED = "challengesCreated";
    public static final String USERS_PHOTO_URI = "photoUri";
    public static final String USERS_CITY = "city";
    public static final String USERS_EMAIL = "email";
    public static final String USERS_SCORE = "score";

    public static final String FOLLOWERS_ENTITY = "followerData";
    public static final String FOLLOWING_ENTITY = "followingData";

    public static final String CHALLENGES_FINISHED_ENTITY = "challengesFinished";
    public static final String CHALLENGES_FINISHED_CHALLENGEID = "challengeID";
    public static final String CHALLENGES_FINISHED_USERID = "userID";
    public static final String CHALLENGES_FINISHED_TIMESTAMP = "timestamp";
    public static final String CHALLENGES_FINISHED_STATUS = "status";

    public static final String ONGOING_ENTITY = "onGoing";
    public static final String ONGOING_CHALLENGE = "challenge";
    public static final String ONGOING_ACTIVITY_STATUS = "activityStatus";
    public static final String ONGOING_START_TIME = "startTime";
    public static final String ONGOING_STATUS = "status";

    public static final String REVIEWS_ENTITY = "reviews";

    // categories and related place types
    private static final Integer[] TRANSPORT = new Integer[] {2,14,18, 41, 70, 81, 89, 91, 92, 1030};
    private static final Integer[] ENTERTAINMENT = new Integer[] {3,4,13, 21, 64, 96};
    private static final Integer[] ART = new Integer[] {5, 12, 55, 66};
    private static final Integer[] FOOD = new Integer[] {7, 15, 38, 43,60, 61, 79};
    private static final Integer[] NIGHTLIFE = new Integer[] {9, 67};
    private static final Integer[] OUTDOOR = new Integer[] {16, 69, 1010};
    private static final Integer[] RELIGIOUS = new Integer[] { 22 , 23, 48, 62, 74, 90};
    private static final Integer[] MEDICAL = new Integer[] { 28, 30, 47, 50, 72, 73};
    private static final Integer[] LODGING = new Integer[] { 59};
    private static final Integer[] SPORT = new Integer[] { 86, 44 };
    private static final Integer[] BEAUTY_CENTER = new Integer[] { 10, 45, 85 };
    private static final Integer[] SHOPPING_MALL = new Integer[] { 84 };
    private static final Integer[] STORE = new Integer[] { 11, 17, 25, 26, 29, 32, 37, 40, 46, 49, 52, 56, 71, 83, 88 };
    private static final Integer[] GOVERNMENT = new Integer[] { 1001, 1002, 1003, 24, 27, 33, 36, 57, 76, 77 };
    private static final Integer[] OTHER = new Integer[] { 1005, 1006, 1007, 1008, 1009, 1011, 1015, 1016, 1017, 1014,
            1019, 1020, 1021, 1027, 1029, 0,1, 6, 8, 35,34, 1018, 1028,
            31, 42, 51, 54, 82, 87,95, 94, 19, 20, 39, 52, 53, 58, 63,65, 68, 75, 78, 80, 93, 95, 1004, 1012};

    // category name list
    public static final String[] CATEGORY_NAMES = new String[] {"Transport", "Entertainment", "Art", "Food", "Nightlife", "Outdoor",
           "Religious", "Medical", "Lodging", "Sport", "BeautyCenter", "ShoppingMall", "Store", "Government", "Other" };
/*
    public static final String[] CATEGORY_NAMES = new String[] {"TRANSPORT", "ENTERTAINMENT", "ART", "FOOD", "NIGHTLIFE", "OUTDOOR",
            "RELIGIOUS", "MEDICAL", "LODGING", "SPORT", "BEAUTY_CENTER", "SHOPPING_MALL", "FINANCIAL", "STORE",
            "ESTABLISHMENT", "PROFESSIONAL", "GOVERNMENT", "OTHER" };*/

    // category list
    public static final Integer[][] CATEGORIES = new Integer[][] {TRANSPORT, ENTERTAINMENT, ART, FOOD, NIGHTLIFE, OUTDOOR,
            RELIGIOUS, MEDICAL, LODGING, SPORT, BEAUTY_CENTER, SHOPPING_MALL, STORE, GOVERNMENT, OTHER };


}
