package com.example.android.travellerchallenge.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.android.travellerchallenge.R;
import com.example.android.travellerchallenge.adapter.CommentAdapter;
import com.example.android.travellerchallenge.constant.ProjectConstants;
import com.example.android.travellerchallenge.entity.ReviewItem;
import com.example.android.travellerchallenge.fragment.ReviewFragment;
import com.example.android.travellerchallenge.util.ProjectMethods;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBufferResponse;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DecimalFormat;
import java.util.ArrayList;

import com.example.android.travellerchallenge.adapter.ActivityAdapter;

import com.example.android.travellerchallenge.entity.ActivityItem;

/**
 * This is the activity to display information of a Challenge such as name, city, rate,rate count,score,
 * duration, creator, played times, activities and comments.
 * The challenge can be started from this page by using the start button.
 * If the challenge is started, it will appended to the Firebase database's onGoing child,
 * then OnGoingChallengeActivity will be created.
 */

public class ChallengePageActivity extends AppCompatActivity {
    private static final String TAG = "ChallengePageActivity";
    private Activity context;
    private boolean hasOnGoingChallenge;

    // TextViews and variables of the challenge's info
    private TextView challengeName, city, rate, rateCount, score, durationTV, playedTimesViev, creator;
    private String creatorID;
    private String challengeID;

    private DatabaseReference mDatabase;

    // Arraylists and adapters for activity and comment list of the challenge
    private ListView listView;
    private ArrayList<ActivityItem> activities=  new ArrayList<>();
    private ArrayList<ReviewItem> comments = new ArrayList<>();
    private ActivityAdapter activityAdapter;
    private CommentAdapter commentAdapter;

    private final FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feed_list);
        context = this;

        // getting bundle from parent activity, which challenge will be displayed.
        Bundle b = getIntent().getExtras();
        challengeID =null;
        if(b != null)
            challengeID = b.getString("id");
        Log.v(TAG, "Created with ID "+ challengeID);

        // initializing list adapters
        activityAdapter = new ActivityAdapter(context,activities);
        commentAdapter = new CommentAdapter(context,comments);

        // Base database reference
        mDatabase = FirebaseDatabase.getInstance().getReference();

        // adding challenge info layout as header to activities and comments lists
        listView = findViewById(R.id.feed_list);
        LayoutInflater layoutinflater = getLayoutInflater();
        ViewGroup header = (ViewGroup) layoutinflater.inflate(R.layout.challenge_page_layout, listView, false);
        listView.addHeaderView(header);

        // initializing TextViews of challenge info header
        challengeName = findViewById(R.id.challenge_name);
        city = findViewById(R.id.challenge_city);
        rate = findViewById(R.id.ratio);
        rateCount = findViewById(R.id.nmbr_of_rates);
        score = findViewById(R.id.score);
        durationTV = findViewById(R.id.duration);
        playedTimesViev = findViewById(R.id.played_times);
        creator = findViewById(R.id.creator);

        // button to start the map activity
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setVisibility(View.VISIBLE);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,MapActivity.class);
                Bundle b = new Bundle();
                b.putString("challengeID", challengeID);
                intent.putExtras(b);
                startActivity(intent);
            }
        });

        // the start button that will be used to start a challenge that will be processed as ongoing
        final Button start = findViewById(R.id.start_button);

        final DatabaseReference onGoingChallengeRef = mDatabase.child(ProjectConstants.ONGOING_ENTITY);
        onGoingChallengeRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (currentUser != null){
                    hasOnGoingChallenge = dataSnapshot.child(currentUser.getUid()).exists();
                }
                start.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(!hasOnGoingChallenge) {
                            // alert button to display message before proceed
                            AlertDialog.Builder builder = new AlertDialog.Builder(context);
                            builder.setTitle(ProjectConstants.CHALLENGE_ME);
                            builder.setMessage(ProjectConstants.START_CHALLENGE_MESSAGE);
                            builder.setPositiveButton(ProjectConstants.POSITIVE_ANSWER, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // start challenge
                                    onStartChallenge();
                                }
                            });
                            builder.setNegativeButton(ProjectConstants.NEGATIVE_ANSWER, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                            builder.show();
                        }
                        else{
                            AlertDialog.Builder builder = new AlertDialog.Builder(context);
                            builder.setTitle(ProjectConstants.HAS_ONGOING_CHALLENGE_TITLE);
                            builder.setMessage(ProjectConstants.HAS_ONGOING_CHALLENGE_MESSAGE);
                            builder.show();
                        }
                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        // retrieving the information of challenge with the ID given by bundle, from firebase by value listener
        final DatabaseReference cRef = mDatabase.child(ProjectConstants.CHALLENGES_ENTITY).child(challengeID);

        cRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    Log.v(TAG, "Challenge Info Received for ID "+ cRef.getKey());
                    challengeName.setText(dataSnapshot.child(ProjectConstants.CHALLENGES_CNAME).getValue(String.class));
                    city.setText(dataSnapshot.child(ProjectConstants.CHALLENGES_CITY).getValue(String.class));

                    Double r = dataSnapshot.child(ProjectConstants.CHALLENGES_RATE).getValue(Double.class);
                    DecimalFormat df = new DecimalFormat("#.#");
                    rate.setText(df.format(r));

                    rateCount.setText(String.valueOf(dataSnapshot.child(ProjectConstants.CHALLENGES_RATECOUNT).getValue(int.class)));
                    score.setText(String.valueOf(dataSnapshot.child(ProjectConstants.CHALLENGES_SCORE).getValue(int.class)));
                    Integer duration = dataSnapshot.child(ProjectConstants.CHALLENGES_DURATION).getValue(int.class);
                    if(duration != null){
                        String d = ProjectMethods.convertDurationToString(duration);
                        durationTV.setText(d);
                    }
                    creatorID = dataSnapshot.child(ProjectConstants.CHALLENGES_CREATORID).getValue(String.class);

                    TextView creationDate = findViewById(R.id.created_date);
                    Long dateInMillis = dataSnapshot.child(ProjectConstants.CHALLENGES_DATE).getValue(Long.class);
                    if(dateInMillis != null){
                        String createdDate = ProjectMethods.convertMillisToDate(-dateInMillis);
                        creationDate.setText(createdDate);
                    }

                    // retrieving the user name with the given userID, from firebase user child
                    DatabaseReference uRef = mDatabase.child(ProjectConstants.USERS_ENTITY).child(creatorID);
                    uRef.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if(dataSnapshot.exists())
                                creator.setText(dataSnapshot.child(ProjectConstants.USERS_USERNAME).getValue(String.class));
                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) { }
        });

        // retrieving the activity list of the displayed challenge, from Firebase database
        DatabaseReference actRef = cRef.child(ProjectConstants.CHALLENGES_ACTIVITIES);
        actRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    activityAdapter.clear();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        if (snapshot.exists()) {
                            // for all activities of the challenge, found the Place by given ID (using Places API)
                            // activities are restored in database by related PlaceID's
                            String PlaceID = snapshot.getValue(String.class);
                            GeoDataClient mGeoDataClient = Places.getGeoDataClient(context, null);
                            mGeoDataClient.getPlaceById(PlaceID).addOnCompleteListener(new OnCompleteListener<PlaceBufferResponse>() {
                                @Override
                                public void onComplete(@NonNull Task<PlaceBufferResponse> task) {
                                    if (task.isSuccessful()) {
                                        // getting first likely place from place buffer
                                        PlaceBufferResponse places = task.getResult();
                                        Place p = places.get(0);
                                        Log.i(TAG, "Place found: " + p.getName());

                                        // add new ActivityItem to adapter, then release the buffer
                                        // Activity Item contains ID, name and type info
                                        activityAdapter.add(new ActivityItem(p.getId(), p.getName().toString(), 0, p.getPlaceTypes()));
                                        places.release();
                                    } else {
                                        Log.e(TAG, "Place not found.");
                                    }
                                }
                            });
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) { }
        });

        // retrieving the information about the number of times played of the displayed challenge
        // this data retrieved from challengesCompleted child from firebase
        DatabaseReference ref = mDatabase.child(ProjectConstants.CHALLENGES_FINISHED_ENTITY);
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    int playedTimes = 0;
                    for(DataSnapshot snapshot:dataSnapshot.getChildren()){
                        if(snapshot != null){
                            if(challengeID.equals(snapshot.child(ProjectConstants.CHALLENGES_FINISHED_CHALLENGEID).getValue(String.class)))
                                playedTimes ++;
                        }
                    }
                    playedTimesViev.setText(String.valueOf(playedTimes));
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) { }
        });

        // retrieving the comment list of the displayed challenge, from Firebase database
        DatabaseReference reviewsEntityRef = mDatabase.child(ProjectConstants.REVIEWS_ENTITY).child(challengeID);
        reviewsEntityRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    commentAdapter.clear();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        if (snapshot.exists()) {
                            ReviewItem c = snapshot.getValue(ReviewItem.class);
                            commentAdapter.add(c);
                        }

                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        // initialize tablayout which contains Activities and Comments items
        final TabLayout tabLayout = findViewById(R.id.tabLayout);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                switch (tab.getPosition()) {
                    case 0:
                        // set listview with activity list
                        listView.setAdapter(activityAdapter);
                        break;

                    case 1:
                        // set listview with comment list
                        listView.setAdapter(commentAdapter);
                        break;
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) { }

            @Override
            public void onTabReselected(TabLayout.Tab tab) { }
        });

        // determining the initial selected tablayout item as activities
        TabLayout.Tab tab = tabLayout.getTabAt(0);
        if(tab != null){
            tab.select();
        }
        listView.setAdapter(activityAdapter);

        // setting onclick listener to listview
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                switch (view.getId()){
                    // if activity item clicked
                    case R.id.activity_item:
                        Log.v(TAG, "Activity item clicked.");
                        break;

                    // if comment item clicked, display information of the comment in dialog fragment
                    case R.id.comment_item:
                        Log.v(TAG, "Review item clicked.");
                        FragmentManager fm = getFragmentManager();
                        ReviewFragment dialogFragment = new ReviewFragment();

                        // dialog fragment to display review
                        Bundle args = new Bundle();
                        args.putString("challengeID", challengeID);
                        args.putString("reviewID", comments.get(position-1).getReviewID());
                        dialogFragment.setArguments(args);
                        dialogFragment.setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomDialog);
                        dialogFragment.show(fm, "Review Fragment");
                        break;
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * method to call when start button clicked
     **/
    private void onStartChallenge(){
        // append the challenge to firebase onGoing child as ongoing challenge
        appendAsOnGoing();
        // intent to ongoing challenge page
        Intent intent = new Intent(context, OnGoingChallengeActivity.class);
        startActivity(intent);
    }

    /**
     * method to append the challenge to firebase onGoing child as ongoing challenge
      */
    private void appendAsOnGoing(){
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if( user != null){
            final String userID = user.getUid();
            // in onGoing child create a child with userID as key,
            // in the child with userID, create a new child named "challenge" and set value to started challengeID
            // in the child with userID, create a new child named "startTime" and set value to system time in millis
            // in the child with userID, create a new child named "activityStatus",
            // then for each activity create a child with the key activity ID
            // and set value to "ONGOING" for the first item and "NOTVISITED" for the rest
            mDatabase.child(ProjectConstants.ONGOING_ENTITY).child(userID).child(ProjectConstants.ONGOING_CHALLENGE).setValue(challengeID);
            Long startTime = System.currentTimeMillis();
            mDatabase.child(ProjectConstants.ONGOING_ENTITY).child(userID).child(ProjectConstants.ONGOING_START_TIME).setValue(startTime);

            DatabaseReference ref = mDatabase.child(ProjectConstants.ONGOING_ENTITY).child(userID).child(ProjectConstants.ONGOING_ACTIVITY_STATUS);
            mDatabase.child(ProjectConstants.ONGOING_ENTITY).child(userID).child(ProjectConstants.ONGOING_ACTIVITY_STATUS).removeValue();
            for(int i=0;i<activities.size();i++) {
                ActivityItem item = activities.get(i);
                Log.v(TAG,item.getID());

                if (i == 0) {
                    item.setStatus(ProjectConstants.ONGOING);
                    //ref.child(item.getID()).setValue(ProjectConstants.ONGOING);
                } else {
                    item.setStatus(ProjectConstants.NOTVISITED);
                    //ref.child(item.getID()).setValue(ProjectConstants.NOTVISITED);
                }
            }
            ref.setValue(activities);
        }
    }

}
