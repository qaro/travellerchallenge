package com.example.android.travellerchallenge.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.example.android.travellerchallenge.activity.UserProfileActivity;
import com.example.android.travellerchallenge.util.CircleTransform;
import com.example.android.travellerchallenge.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import com.example.android.travellerchallenge.entity.User;

/**
 * This class is recycler view adapter class with for user items.
 * That will perform the display of the user object attributes.
 */

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.SearchViewHolder> {

    private Context context;
    private ArrayList<User> userList;

    // ViewHolder Class
    class SearchViewHolder extends RecyclerView.ViewHolder {

        ImageView profileImage;
        TextView username;
        TextView city;
        TextView score;
        private RelativeLayout relativeLayout;

        SearchViewHolder(View itemView){
            super(itemView);
            profileImage = itemView.findViewById(R.id.friend_pi);
            username = itemView.findViewById(R.id.username);
            city = itemView.findViewById(R.id.user_city);
            score =  itemView.findViewById(R.id.user_score);
            relativeLayout = itemView.findViewById(R.id.friend_item);
        }

    }

    public SearchAdapter(Context context, ArrayList<User> userList) {
        this.context = context;
        this.userList = userList;
    }

    @Override
    public SearchAdapter.SearchViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.userlist_item,parent,false);
        return new SearchAdapter.SearchViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SearchViewHolder holder, final int position) {
        // update UI
        Uri photoUri = Uri.parse(userList.get(position).getPhotoUri());
        holder.username.setText(userList.get(position).getUsername());
        holder.city.setText(userList.get(position).getCity());
        holder.score.setText(String.valueOf(userList.get(position).getScore()));
        Picasso.get().load(photoUri).transform(new CircleTransform()).into(holder.profileImage);

        // add listener to user view items
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // intent to clicked user's UserProfileActivity
                Intent intent = new Intent(context, UserProfileActivity.class);
                Bundle b = new Bundle();
                b.putString("userid", userList.get(position).getUserID());
                intent.putExtras(b);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return userList.size();
    }
}
