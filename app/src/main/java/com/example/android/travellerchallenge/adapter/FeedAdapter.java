package com.example.android.travellerchallenge.adapter;

import android.app.Activity;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.android.travellerchallenge.entity.FeedItem;
import com.example.android.travellerchallenge.R;
import com.example.android.travellerchallenge.util.CircleTransform;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * This class is array adapter class with for feed items.
 * That will perform the display of the feed object attributes.
 */

public class FeedAdapter extends ArrayAdapter<FeedItem> {
    public FeedAdapter(Activity context, ArrayList<FeedItem> feeds){
        super(context,0,feeds);
    }

    @Override
    @NonNull
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {

        View listItemView = convertView;
        // Check if the existing view is being reused, otherwise inflate the view
        if(listItemView == null){
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.feed_item,parent,false);
        }

        FeedItem currentFeed = getItem(position);
        if (currentFeed != null){
            TextView userName =  listItemView.findViewById(R.id.username_textview);
            userName.setText(currentFeed.getUserName());

            TextView feedContent =  listItemView.findViewById(R.id.feed_textview);
            feedContent.setText(currentFeed.getFeedsContent());

            ImageView profileImage =  listItemView.findViewById(R.id.profile_image);
            Picasso.get().load(Uri.parse(currentFeed.getProfileImageID())).transform(new CircleTransform()).into(profileImage);
            profileImage.setVisibility(View.VISIBLE);

        }
        return listItemView;
    }

}
