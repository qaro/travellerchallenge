package com.example.android.travellerchallenge.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import com.example.android.travellerchallenge.constant.ProjectConstants;

import com.example.android.travellerchallenge.R;

import java.util.ArrayList;

/**
 * This is the fragment which be loaded to selection activity, and contains fields to permit user
 * to enter preferences, such as categories.
 * This fragment contains interface to communicate with SelectionActivity
 */

public class StepTwoFragment extends Fragment {

    private PassCategory passCategory;
    private ArrayList<String> categories = new ArrayList<>();


    public StepTwoFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.selection_step2, container, false);
        Activity context = getActivity();

        // add checkboxes programmatically
        LinearLayout linearLayout = rootView.findViewById(R.id.checkbox_layout);
        for (String i : ProjectConstants.CATEGORY_NAMES){
            final CheckBox checkBox = new CheckBox(context);
            checkBox.setText(i);
            linearLayout.addView(checkBox);

            // set listener to checkboxes to update categories list
            checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean checked = checkBox.isChecked();
                    if (checked) {
                        categories.add(checkBox.getText().toString().trim());
                    } else {
                        categories.remove(checkBox.getText().toString().trim());
                    }
                }
            });
        }

        passCategory = (PassCategory) context;
        if (passCategory != null){
            passCategory.passCategoryData(categories);
        }
        return rootView;
    }

    @Override
    public void onResume() {
        passCategory.passCategoryData(categories);
        super.onResume();
    }

    /**
     * interface to pass the categories selected by user
      */
    public interface PassCategory{
        void passCategoryData(ArrayList<String> categories);
    }
}
