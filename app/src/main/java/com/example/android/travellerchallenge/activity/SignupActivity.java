package com.example.android.travellerchallenge.activity;

import android.app.ProgressDialog;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.travellerchallenge.R;
import com.example.android.travellerchallenge.constant.ProjectConstants;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;


import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import com.example.android.travellerchallenge.entity.User;

/**
 * This is the activity to sign up the system. The user can sign up with Firebase authentication.
 * The login activity can be start from this activity by intent.
 * If sign up success, first login activity will be started by intent than main activity will be started.
 * If sign up success, user will be appended to database
 */

public class SignupActivity extends AppCompatActivity {

    private Button singupButton;
    private TextView signupUsername ;
    private TextView signupEmail;
    private TextView signupPassword;
    private String emailInput;

    private FirebaseAuth uAuth;

    private DatabaseReference mDatabase ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_layout);

        // initializing views
        signupUsername =  findViewById(R.id.input_name);
        signupEmail =  findViewById(R.id.input_email);
        signupPassword = findViewById(R.id.input_password);
        singupButton =  findViewById(R.id.signup_button);
        TextView linkLogin =  findViewById(R.id.link_login);

        uAuth = FirebaseAuth.getInstance();

        mDatabase = FirebaseDatabase.getInstance().getReference();

        // set onClickedlistener to signup button to perform signup action
        singupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signupAction();
            }
        });

        // setting click listener to intent to login activity
        linkLogin.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                finish();
            }
        });
    }

    /**
     * method to perform sign up action via email Firebase Auth
      */
    private void signupAction () {

        // validate user input
        if (!validation()) {
            signupFailed();
            return;
        }

        // disable sing up button to avoid multiple click
        singupButton.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(SignupActivity.this);

        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Creating ...");
        progressDialog.show();

        // get user input
        emailInput = signupEmail.getText().toString().trim();
        String passwordInput = signupPassword.getText().toString().trim();

        // authenticate with firebase
        uAuth.createUserWithEmailAndPassword(emailInput, passwordInput).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    // sign up succeed
                    signupSuccess(task.getResult().getUser());
                    progressDialog.dismiss();
                }
                else{
                    // sign uğ failed
                    signupFailed();
                    progressDialog.dismiss();
                }
            }
        });

    }

    /**
     * method to append user to database
      */
    private void appendUser(String userId, String username, String email, String photoUri) {
        // create new user object and append it to database
        User user = new User(userId, username, email, 0, "", photoUri);
        mDatabase.child(ProjectConstants.USERS_ENTITY).child(userId).setValue(user);
    }

    /**
     * method to call when sign up success
      */
    private void signupSuccess(FirebaseUser user){

        String usernameInput = signupUsername.getText().toString().trim();
        FirebaseUser u = FirebaseAuth.getInstance().getCurrentUser();
        Uri photoUri = null;
        if(u != null){
            photoUri = u.getPhotoUrl();
        }
        // convert photo Uri to string
        String photoUriString;
        if(photoUri != null)
            photoUriString = photoUri.toString();
        else
            photoUriString = "";

        // call append user method to database
        appendUser(user.getUid(), usernameInput, emailInput, photoUriString);

        Toast toast = Toast.makeText(this, ProjectConstants.SIGNUP_SUCCESSFUL , Toast.LENGTH_LONG);
        toast.show();

        // enable button again
        singupButton.setEnabled(true);
        setResult(RESULT_OK, null);
        finish();
    }

    /**
     * method to call when sign up failed
      */
    private void signupFailed(){
        Toast toast = Toast.makeText(this, ProjectConstants.SIGNUP_FAILED, Toast.LENGTH_LONG);
        toast.show();
        singupButton.setEnabled(true);
    }

    /**
     * validate user input
      */
    private boolean validation(){

        boolean validInput = true;
        String emailInput = signupEmail.getText().toString();
        String passwordInput = signupPassword.getText().toString();
        String usernameInput = signupUsername.getText().toString();

        // username required
        if (usernameInput.isEmpty() || usernameInput.length() < 3) {
            signupUsername.setError(ProjectConstants.USERNAME_SIZE_REQUIRED);
            validInput = false;

        } else {
            signupUsername.setError(null);
        }

        // email Required in email format
        if (emailInput.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(emailInput).matches()) {
            signupEmail.setError(ProjectConstants.EMAIL_REQUIRED);
            signupEmail.requestFocus();
            validInput =false;

        } else {
            signupEmail.setError(null);
        }

        // password is required and must be larger than 6 characters
        if (passwordInput.isEmpty() || passwordInput.length() < 6) {
            signupPassword.setError(ProjectConstants.PASSWORD_SIZE_REQUIRED);
            validInput =false;
        } else {
            signupPassword.setError(null);
        }

        return validInput;
    }

}
