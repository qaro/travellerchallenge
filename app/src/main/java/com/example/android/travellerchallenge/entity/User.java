package com.example.android.travellerchallenge.entity;

/**
 * Created by basaragakadi on 4.03.2018.
 * This is the User Class.
 * User is basically the authenticated persons to the application with attributes.
 */

public class User {

    private String userID;
    private String username;
    private String eMail;
    private long score;
    private String city;
    private String photoUri;

    public User() {
        this.userID = "";
        this.username = "";
        this.eMail = "";
        this.score = 0;
        this.city = "";
        this.photoUri = "";
    }

    public User(String username, String email, long score, String photoUri) {

        this.username = username;
        this.eMail = email;
        this.score = score;
        this.city = "";
        this.photoUri = photoUri;
    }

    public User(String userID, String username, String email, long score, String city, String photoUri) {

        this.userID = userID;
        this.username = username;
        this.eMail = email;
        this.score = score;
        this.city = city;
        this.photoUri = photoUri;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return eMail;
    }

    public long getScore() {
        return score;
    }

    public String getCity() {
        return city;
    }

    public String getPhotoUri() {
        return photoUri;
    }

    public String getUserID() {
        return userID;
    }

    public void setUsername(String uName) {
        this.username = uName;
    }

    public void setEmail(String email) {
        this.eMail = email;
    }

    public void setScore(long score) {
        this.score = score;
    }

    public void setPhotoUri(String photoUri){
        this.photoUri = photoUri;
    }

    public void setUserID(String userID){
        this.userID = userID;
    }
}
