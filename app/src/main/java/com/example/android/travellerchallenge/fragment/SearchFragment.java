package com.example.android.travellerchallenge.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.android.travellerchallenge.R;
import com.example.android.travellerchallenge.constant.ProjectConstants;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import com.example.android.travellerchallenge.adapter.SearchAdapter;
import com.example.android.travellerchallenge.entity.User;

/**
 * This is the fragment which be loaded to main activity, and display the users by searched username.
 * User can perform search operations from this fragment,
 * then the users with username that contains search string will be displayed.
 */

public class SearchFragment extends Fragment {
    public SearchFragment() {
        // Required empty public constructor
    }
    private Activity context;


    private SearchAdapter searchAdapter;

    private DatabaseReference usersDatabase;

    private String username;
    private String eMail;
    private String city;
    private Long score;
    private String userID;
    private String photoUri;

    private ArrayList<User> userList;

    private RecyclerView resultsView;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        context = getActivity();

        usersDatabase = FirebaseDatabase.getInstance().getReference().child(ProjectConstants.USERS_ENTITY);

        final View rootView = inflater.inflate(R.layout.search_layout, container, false);
        setHasOptionsMenu(true);

        userList = new ArrayList<>();
        EditText searchField = rootView.findViewById(R.id.search_edit_text);
        resultsView = rootView.findViewById(R.id.recycler_view);

        // configure resultView
        resultsView.hasFixedSize();
        resultsView.setLayoutManager(new LinearLayoutManager(context));
        resultsView.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));

        // add text change listener to get user input for search
        searchField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().isEmpty()){
                    // if any user input exist, set adapter for this search string
                    setAdapter(s.toString());
                } else{
                    // else clear adapter
                    userList.clear();
                    resultsView.removeAllViews();
                }
            }
        });

        return rootView;
    }

    /**
     * method to setAdapter to result view according to searchedString from user input
      */
    private void setAdapter(final String searchedString){

        context = getActivity();
        // listener to get results as user objects
        usersDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    userList.clear();
                    resultsView.removeAllViews();

                    // get user attributes of all users in the result
                    for(DataSnapshot snapshot : dataSnapshot.getChildren()){

                        userID = snapshot.child("userID").getValue(String.class);
                        username = snapshot.child("username").getValue(String.class);
                        city = snapshot.child("city").getValue(String.class);
                        eMail = snapshot.child("mEmail").getValue(String.class);
                        score = snapshot.child("score").getValue(Long.class);
                        photoUri = snapshot.child("photoUri").getValue(String.class);

                        // if username contains the search username, then add user to list
                        if(username.toLowerCase().contains(searchedString.toLowerCase())){
                            userList.add(new User(userID, username, eMail, score, city, photoUri));
                        }

                    }
                    // set adapter to result view
                    searchAdapter = new SearchAdapter(context, userList);
                    resultsView.setAdapter(searchAdapter);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

}

