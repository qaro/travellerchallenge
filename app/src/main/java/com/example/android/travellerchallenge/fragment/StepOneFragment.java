package com.example.android.travellerchallenge.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.app.Fragment;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.android.travellerchallenge.R;
import com.example.android.travellerchallenge.constant.ProjectConstants;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceDetectionClient;
import com.google.android.gms.location.places.PlaceLikelihoodBufferResponse;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;

/**
 * This is the fragment which be loaded to selection activity, and contains fields to permit user
 * to enter preferences, such as start and end place, start and end date of the challenges.
 * This fragment contains interface to communicate with SelectionActivity
 */

public class StepOneFragment extends Fragment implements ActivityCompat.OnRequestPermissionsResultCallback{

    private static final String TAG = "StepOneFragment";

    Activity context;

    // TextViews of start and end place
    private TextView startPoint;
    private TextView destinationPoint ;

    // Current place of the device, selected start and end place
    private Place currentPlace, startPlace, endPlace;

    private String currentPlaceName, startPlaceName, endPlaceName = "Optional";

    // Selected start and end time
    private Calendar startDate = Calendar.getInstance();
    private Calendar endDate =Calendar.getInstance();

    private int year, month, day, hour, minute;

    // Related views of the start and end time
    private TextView startDateView;
    private TextView endDateView;
    private TextView startTimeView;
    private TextView endTimeView;

    // View id, to specify which date and time view has been clicked
    private int vid = 0;

    // Interface for pass data to parent activity
    private PassPlaceDate passPlaceDate;

    private PlaceDetectionClient mPlaceDetectionClient;

    private boolean mLocationPermissionGranted = false;

    public StepOneFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.selection_step1, container, false);

        context = getActivity();

        // initialize the interface
        passPlaceDate = (PassPlaceDate) context;

        // Textview's of the start and end place
        destinationPoint = rootView.findViewById(R.id.destination_point);
        startPoint = rootView.findViewById(R.id.start_point);

        // Initialize the PlaceDetectionClient
        mPlaceDetectionClient = Places.getPlaceDetectionClient(context, null);

        // If the start place has not been chosen yet, set current place as start place
        if(startPlaceName == null){
            // Function for enable location and get current place
            enableMyLocation();
        }

        // Display start and end place
        startPoint.setText(startPlaceName);
        destinationPoint.setText(endPlaceName);

        // Set click listener with place picker
        startPoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    // Create place picker
                    PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                    Intent intent = builder.build(context);
                    // Start picker for origin place request
                    startActivityForResult(intent, ProjectConstants.ORIGIN_PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException e) {
                    GooglePlayServicesUtil
                            .getErrorDialog(e.getConnectionStatusCode(), context, 0);
                } catch (GooglePlayServicesNotAvailableException e) {
                    Toast.makeText(context, ProjectConstants.GOOGLE_PLAY_SERVICES_NOT_AVAILABLE,
                            Toast.LENGTH_LONG)
                            .show();
                } catch (Exception e){
                    Toast.makeText(context, "Error" + e.getMessage(),
                            Toast.LENGTH_LONG)
                            .show();
                }
            }
        });

        // Set click listener with place picker
        destinationPoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    // Create place picker
                    PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                    Intent intent = builder.build(context);
                    // Start picker for destination place request
                    startActivityForResult(intent, ProjectConstants.DESTINATION_PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException e) {
                    GooglePlayServicesUtil
                            .getErrorDialog(e.getConnectionStatusCode(), context, 0);
                } catch (GooglePlayServicesNotAvailableException e) {
                    Toast.makeText(context, ProjectConstants.GOOGLE_PLAY_SERVICES_NOT_AVAILABLE,
                            Toast.LENGTH_LONG)
                            .show();
                } catch (Exception e){
                    Toast.makeText(context, "Error:" + e.getMessage(),
                            Toast.LENGTH_LONG)
                            .show();
                }

            }
        });

        // TextViews of start and end dates
        startDateView = rootView.findViewById(R.id.start_date);
        endDateView = rootView.findViewById(R.id.end_date);
        startTimeView = rootView.findViewById(R.id.start_time);
        endTimeView = rootView.findViewById(R.id.end_time);

        // Variables of start date to create Date and Time Pickers
        year = startDate.get(Calendar.YEAR);
        month = startDate.get(Calendar.MONTH);
        day = startDate.get(Calendar.DAY_OF_MONTH);
        hour = startDate.get(Calendar.HOUR_OF_DAY);
        minute = startDate.get(Calendar.MINUTE);

        // Show DatePicker dialog if start date clicked
        startDateView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vid = R.id.start_date;
                DatePickerDialog dialog = new DatePickerDialog(context, myDateListener, year, month, day);
                dialog.getDatePicker().setMinDate(Calendar.getInstance().getTimeInMillis() -10000);
                dialog.show();
            }
        });

        // Show DatePicker dialog if end date clicked
        endDateView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vid = R.id.end_date;
                DatePickerDialog dialog = new DatePickerDialog(context, myDateListener, year, month, day);
                dialog.getDatePicker().setMinDate(Calendar.getInstance().getTimeInMillis() -10000);
                dialog.show();
            }
        });

        // Show TimePicker dialog if start time clicked
        startTimeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vid = R.id.start_time;
                TimePickerDialog dialog = new TimePickerDialog(context, myTimeListener,hour, minute, DateFormat.is24HourFormat(context));
                dialog.show();
            }
        });

        // Show TimePicker dialog if end time clicked
        endTimeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vid = R.id.end_time;
                TimePickerDialog dialog = new TimePickerDialog(context, myTimeListener,hour, minute, DateFormat.is24HourFormat(context));
                dialog.show();
            }
        });

        // Display start and end date information
        showDate(startDate, endDate);

        return rootView;

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // if start place picker is returned, set start place
        if (requestCode == ProjectConstants.ORIGIN_PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(context,data);
                startPlaceName = String.valueOf(place.getName());
                startPoint.setText(startPlaceName);
                startPlace = place;

            }
        }
        // if end place picker is returned, set end place
        else if (requestCode == ProjectConstants.DESTINATION_PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(context, data);
                endPlaceName = String.valueOf(place.getName());
                destinationPoint.setText(endPlaceName);
                endPlace = place;
            }
        }
        // Pass place data to SelectionActivity
        passPlaceDate.passPlaceData(startPlace,endPlace);
    }

    /**
     * method to enable location
      */
    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Access to the location has not been granted to the app. Prompt the user for permission.
            getLocationPermission();
            Log.i(TAG, "Permission needed");
        } else {
            // Access to the location has been granted to the app. Then get current place of the device
            Log.i(TAG, "Permission granted");
            mLocationPermissionGranted = true;
            getCurrentPlace();
        }
    }

    /**
     *  method to get current place
     */

    private void getCurrentPlace() {
        // Get the likely place - POI that is the best match for the device's current location.
        try {
            if (mLocationPermissionGranted) {

                @SuppressWarnings("MissingPermission") final
                Task<PlaceLikelihoodBufferResponse> placeResult =
                        mPlaceDetectionClient.getCurrentPlace(null);
                placeResult.addOnCompleteListener(new OnCompleteListener<PlaceLikelihoodBufferResponse>() {
                    @Override
                    public void onComplete(@NonNull Task<PlaceLikelihoodBufferResponse> task) {
                        try {
                            PlaceLikelihoodBufferResponse likelyPlaces = task.getResult();
                            Log.i(TAG, String.format("Current place '%s' has likelihood: %g",
                                    likelyPlaces.get(0).getPlace().getName(),
                                    likelyPlaces.get(0).getLikelihood()));

                            // Set current place, then set start place as current place
                            currentPlace = likelyPlaces.get(0).getPlace().freeze();
                            currentPlaceName = String.valueOf(likelyPlaces.get(0).getPlace().getName());
                            startPlaceName = currentPlaceName;
                            startPlace = currentPlace;
                            startPoint.setText(currentPlaceName);

                            // Pass place data to SelectionActivity
                            passPlaceDate.passPlaceData(currentPlace,null);
                            likelyPlaces.release();

                        } catch (Exception e){
                            Log.e("Exception: %s", e.getMessage());
                        }
                    }
                });

            } else {
                // The user has not granted permission.
                Log.i(TAG, "The user did not grant location permission.");
                // Prompt the user for permission.
                getLocationPermission();
            }
        } catch (SecurityException e)  {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    /**
     * methot to grant location permission
      */
    private void getLocationPermission() {
    /*
     * Request location permission, so that we can get the location of the
     * device. The result of the permission request is handled by a callback,
     * onRequestPermissionsResult.
     */
        Log.i(TAG, "getLocationPermission");
        if (ContextCompat.checkSelfPermission(context.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(context,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    ProjectConstants.PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            Log.i(TAG, "getLocationPermission:" + mLocationPermissionGranted);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        Log.i(TAG, "onRequestPermission");
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case ProjectConstants.PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                }
            }
        }
    }

    /**
     * method to display date in a date format
      */
    private void showDate(Calendar start, Calendar end) {

        // Date and time formats to be displayed
        String dateFormat = "dd/MM/yy";
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.US);

        String timeFormat = "HH:mm";
        SimpleDateFormat stf = new SimpleDateFormat(timeFormat, Locale.US);

        // Display dates and times
        startDateView.setText(sdf.format(start.getTime()));
        endDateView.setText(sdf.format(end.getTime()));

        startTimeView.setText(stf.format(start.getTime()));
        endTimeView.setText(stf.format(end.getTime()));

        // Pass date data to SelectionActivity
        passPlaceDate.passDateData(start,end);

    }

    /**
     * Defining our own DatePickerDialogListener
      */
    private DatePickerDialog.OnDateSetListener myDateListener = new
            DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                      int dayOfMonth) {
                    // For clicked view, set date
                    if(vid == R.id.start_date){
                        startDate.set(Calendar.YEAR, year);
                        startDate.set(Calendar.MONTH, monthOfYear);
                        startDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);}
                    else if (vid == R.id.end_date){
                        endDate.set(Calendar.YEAR, year);
                        endDate.set(Calendar.MONTH, monthOfYear);
                        endDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);}

                    // Display date and time
                    showDate(startDate,endDate);

                    // Validate date and time
                    validationDateTime();
                }
            };

    /**
     * Defining our own TimePickerDialogListener
      */
    private TimePickerDialog.OnTimeSetListener myTimeListener = new
            TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    // For clicked view, set time
                    if(vid == R.id.start_time){
                        startDate.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        startDate.set(Calendar.MINUTE,minute);}
                    else if (vid == R.id.end_time){
                        endDate.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        endDate.set(Calendar.MINUTE,minute);}
                    // Display date and time
                    showDate(startDate,endDate);
                    // Validate date and time
                    validationDateTime();
                }
            };

    /**
     * method to validate Date
      */
    private boolean validationDateTime(){
        boolean validInput = true;

        // seconds between start and end date
        long seconds = (endDate.getTimeInMillis() - startDate.getTimeInMillis()) / 1000;

        // The end date cannot be before or equal to start time
        if(seconds<=0){
            validInput=false;
            endTimeView.setError("Please enter valid time interval");
        } else {
            endTimeView.setError(null);
        }

        return validInput;
    }

    /**
     * Interface to pass to place and date info to activity
      */
    public interface PassPlaceDate{
        void passDateData(Calendar start, Calendar end);
        void passPlaceData(Place start, Place end);
    }

}


