package com.example.android.travellerchallenge.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.android.travellerchallenge.constant.ProjectConstants;
import com.example.android.travellerchallenge.util.CircleTransform;
import com.example.android.travellerchallenge.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.IOException;

/**
 * This is the activity to edit the user profile information such as image, name, city, and password.
 * The challenges can be saves from this page by using save menu item button.
 * If the changes are made, they will appended to the Firebase database's current user child.
 * Image will be loaded by using android.provider.MediaStore and UploadTask
 * Password will be changed by using Firebase AuthCredential reauthenticate method
 */

public class EditProfileActivity extends AppCompatActivity{

    private static final String TAG = "EditProfileActivity";

    // Uri of loaded profile image
    private Uri filePath;
    private Uri downloadUrl;

    private String email;

    private FirebaseUser currentUser;
    private DatabaseReference databaseReference;

    private ImageView profileImage;
    private EditText usernameView, cityView, currentPasswordView, newPasswordView,confirmPassWordView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(ProjectConstants.EDIT_PROFILE);
        setContentView(R.layout.edit_page_layout);

        // initialize view items
        usernameView = findViewById(R.id.input_name);
        cityView = findViewById(R.id.input_city);
        currentPasswordView = findViewById(R.id.current_password);
        newPasswordView = findViewById(R.id.new_password);
        confirmPassWordView = findViewById(R.id.confirm_password);
        profileImage = findViewById(R.id.profile_image);

        // Get current user
        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        // get database instances
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();
        // getting current users database reference
        DatabaseReference mUsers = databaseReference.child(ProjectConstants.USERS_ENTITY).child(currentUser.getUid());
        // displaying current users info on the edit activity
        mUsers.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    Uri photoUri = Uri.parse(dataSnapshot.child(ProjectConstants.USERS_PHOTO_URI).getValue(String.class));
                    Picasso.get().load(photoUri).transform(new CircleTransform()).into(profileImage);
                    String username = dataSnapshot.child(ProjectConstants.USERS_USERNAME).getValue(String.class);
                    usernameView.setText(username);
                    String city = dataSnapshot.child(ProjectConstants.USERS_CITY).getValue(String.class);
                    cityView.setText(city);
                    // getting email of user in order to use in password change process
                    email = dataSnapshot.child(ProjectConstants.USERS_EMAIL).getValue(String.class );
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                // required override method
                }
        });

        // Setting onClickListener to profile image view
        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // intent to choose a media from photos
                Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, ProjectConstants.RESULT_LOAD_IMAGE);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        // Adding new menu item
        MenuItem item = menu.add(Menu.NONE, ProjectConstants.MENU_ITEM_SAVE,Menu.NONE,"Save");
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            case ProjectConstants.MENU_ITEM_SAVE:
                // check if user inputs are valid
                if(validation()){
                    onSaveClicked(this);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // Override method of the result of the RESULT_LOAD_DATA
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // if request load data is resulted with success, load selected image and change profileImage view
        if(requestCode == ProjectConstants.RESULT_LOAD_IMAGE && resultCode == RESULT_OK && data != null && data.getData() != null ){
            // get selected file Uri
            filePath = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                CircleTransform c = new CircleTransform();
                bitmap = c.transform(bitmap);
                profileImage.setImageBitmap(bitmap);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    /**
     * method to call when save menu item clicked
     */
    private void onSaveClicked(final Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(ProjectConstants.SAVE_CHANGES);
        builder.setMessage(ProjectConstants.SAVE_CHANGES_MESSAGE);
        builder.setPositiveButton(ProjectConstants.POSITIVE_ANSWER, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // save changes
                saveChanges();
            }
        });
        builder.setNegativeButton(ProjectConstants.NEGATIVE_ANSWER, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) { }
        });
        builder.show();
    }

    /**
     * method to change information of the user in the database
      */
    private void saveChanges(){
        // update username if changed
        databaseReference.child(ProjectConstants.USERS_ENTITY).child(currentUser.getUid()).child(ProjectConstants.USERS_USERNAME).setValue(usernameView.getText().toString());
        databaseReference.child(ProjectConstants.USERS_ENTITY).child(currentUser.getUid()).child(ProjectConstants.USERS_CITY).setValue(cityView.getText().toString());
        Snackbar.make(findViewById(R.id.linear_layout), ProjectConstants.CHANGES_SAVED, Snackbar.LENGTH_LONG).show();
        // upload image if selected
        changePhoto();
        // change password if selected
        changePassword();
    }

    /**
    * method to call to change password
     */
    private void changePassword(){
        String currentPassword = currentPasswordView.getText().toString();
        final String newPassword = newPasswordView.getText().toString();
        // break early if no changes is required
        if(currentPassword.equals("")) {return;}

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Updating password...");
        progressDialog.show();
        // get Firebase Authentication credential, and re-authenticate
        AuthCredential credential = EmailAuthProvider.getCredential(email, currentPassword);
        currentUser.reauthenticate(credential).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    // task to change password
                    currentUser.updatePassword(newPassword).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Snackbar.make(findViewById(R.id.linear_layout), ProjectConstants.PASSWORD_CHANGE_SUCCESSFUL, Snackbar.LENGTH_LONG).show();
                                Log.d(TAG, "Password changed.");
                            } else {
                                Snackbar.make(findViewById(R.id.linear_layout), ProjectConstants.PASSWORD_CHANGE_FAILED, Snackbar.LENGTH_LONG).show();
                                Log.d(TAG, "Error password not updated");
                            }
                        }
                    });
                    progressDialog.dismiss();
                } else {
                    Snackbar.make(findViewById(R.id.linear_layout), ProjectConstants.CURRENT_PASSWORD_ERROR, Snackbar.LENGTH_LONG).show();
                    Log.d(TAG, "Error auth failed");
                    progressDialog.dismiss();

                }
            }
        });
    }

    /**
    * method to call to upload the selected photo to Firebase Storage, and update user photoUri
     */
    private void changePhoto(){
        if(filePath != null) {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading photo...");
            progressDialog.show();
            // create Firebase Storage instances
            FirebaseStorage storage = FirebaseStorage.getInstance();
            StorageReference storageReference = storage.getReference();

            // upload image by Uri
            StorageReference ref = storageReference.child("images/"+filePath.getLastPathSegment());
            UploadTask uploadTask = ref.putFile(filePath);
            // Handle unsuccessful uploads
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.v(TAG,"Image upload failure");
                    Snackbar.make(findViewById(R.id.linear_layout), ProjectConstants.IMAGE_UPLOAD_FAILED, Snackbar.LENGTH_LONG).show();
                    progressDialog.dismiss();
                }
            });
            // Handle successful uploads
            uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    progressDialog.dismiss();
                    Log.v(TAG,"Image upload success");
                    // get the Uri of the loaded image in Firebase Storage
                    downloadUrl = taskSnapshot.getDownloadUrl();
                    // set current user's Photo Uri value
                    databaseReference.child(ProjectConstants.USERS_ENTITY).child(currentUser.getUid()).child(ProjectConstants.USERS_PHOTO_URI).setValue(downloadUrl.toString());
                    Snackbar.make(findViewById(R.id.linear_layout), ProjectConstants.IMAGE_UPLOAD_SUCCESSFUL, Snackbar.LENGTH_LONG).show();
                }
            });
        }
    }

    /**
     * method to validate user input
     */
    private boolean validation() {

        boolean validInput = true;

        String newPassword = newPasswordView.getText().toString();
        String currentPassword = currentPasswordView.getText().toString();
        String confirmPassword = confirmPassWordView.getText().toString();
        String username = usernameView.getText().toString();

        // if current password is entered
        if(!currentPassword.isEmpty()){
            // new password must be confirmed
            if (confirmPassword.isEmpty() || !confirmPassword.equals(newPassword)) {
                confirmPassWordView.setError(ProjectConstants.CONFIRMATION_REQUIRED);
                validInput = false;
            } else {
                confirmPassWordView.setError(null);
            }
            // new password must be larger than 6 characters
            if (newPassword.length() < 6) {
                newPasswordView.setError(ProjectConstants.PASSWORD_SIZE_REQUIRED);
                validInput = false;
            } else {
                newPasswordView.setError(null);
            }
        } else {
            // if new password is entered, current password must be entered.
            if (!newPassword.isEmpty()) {
                currentPasswordView.setError(ProjectConstants.CURRENT_PASSWORD_REQUIRED);
                validInput = false;
            } else {
                currentPasswordView.setError(null);
            }
        }
        // username must be larger than 3 characters
        if (username.isEmpty() || username.length() < 3) {
            usernameView.setError(ProjectConstants.USERNAME_SIZE_REQUIRED);
            validInput = false;
        } else {
            usernameView.setError(null);
        }
        return validInput;
    }
}
