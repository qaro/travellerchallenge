package com.example.android.travellerchallenge.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.android.travellerchallenge.adapter.FeedAdapter;
import com.example.android.travellerchallenge.constant.ProjectConstants;
import com.example.android.travellerchallenge.entity.FeedItem;
import com.example.android.travellerchallenge.R;
import com.example.android.travellerchallenge.interfacePackage.OnGoingChallengeCallback;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class FeedFragment extends Fragment {
    public FeedFragment() {
        // Required empty public constructor
    }
    private static final String TAG = "MyActivity";

    private ArrayList<FeedItem> tmpFeedItems;
    private FeedAdapter feedAdapter;

    private final DatabaseReference onGoingChallengeReference = FirebaseDatabase.getInstance().getReference().child(ProjectConstants.ONGOING_ENTITY);
    /*
    private final DatabaseReference followingDataReference = FirebaseDatabase.getInstance().getReference().child(ProjectConstants.FOLLOWING_ENTITY).child(currentUser.getUid());
    private final DatabaseReference usersReference = FirebaseDatabase.getInstance().getReference().child(ProjectConstants.USERS_ENTITY);
    private final DatabaseReference challengeReference = FirebaseDatabase.getInstance().getReference().child(ProjectConstants.CHALLENGES_ENTITY);
    */
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.feed_list, container, false);

        ArrayList<FeedItem> feedItems = new ArrayList<>();
        tmpFeedItems = new ArrayList<>();

        readOnGoingChallengesData(new OnGoingChallengeCallback() {
            @Override
            public void onCallbackOnGoingChallenge(ArrayList<FeedItem> feedItemArrayList) {
                for(int i = 0; i < feedItemArrayList.size(); i++) {
                    feedAdapter.add(feedItemArrayList.get(i));
                }
            }
        });

/*
        onGoingChallengeReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                final String userId = dataSnapshot.getKey();
                final String challengeId = dataSnapshot.child(ProjectConstants.ONGOING_CHALLENGE).getValue(String.class);
                followingDataReference.child(userId).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if(dataSnapshot.exists()){

                            usersReference.child(userId).addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    final String photoUriString = dataSnapshot.child(ProjectConstants.USERS_PHOTO_URI).getValue(String.class);
                                    final String username = dataSnapshot.child(ProjectConstants.USERS_USERNAME).getValue(String.class);

                                    challengeReference.child(challengeId).addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            final String challengeTitle = dataSnapshot.child(ProjectConstants.CHALLENGES_CNAME).getValue(String.class);
                                            feedAdapter.add(new FeedItem(username,ProjectConstants.USER_STARTED_CHALLENGE_MESSAGE + challengeTitle, photoUriString));
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {

                                        }
                                    });

                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
*/
        feedAdapter = new FeedAdapter(getActivity(), feedItems);

        ListView listView =  rootView.findViewById(R.id.feed_list);

        listView.setAdapter(feedAdapter);

        return rootView;
    }

    /**
     * Display ongoing challenge feed if a user starts play a challenge
     */
    private void readOnGoingChallengesData(final OnGoingChallengeCallback onGoingChallengeCallback){
        onGoingChallengeReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Long time = snapshot.child(ProjectConstants.ONGOING_START_TIME).getValue(long.class);
                    if (time != null){
                        tmpFeedItems.add(new FeedItem(snapshot.child(ProjectConstants.ONGOING_CHALLENGE).getValue(String.class),
                                "Puan kazandi.", "",time));
                    }
                }
                onGoingChallengeCallback.onCallbackOnGoingChallenge(tmpFeedItems);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "onCancelled: ",databaseError.toException());
            }
        });
    }


}

