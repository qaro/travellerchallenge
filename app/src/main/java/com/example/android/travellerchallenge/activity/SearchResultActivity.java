package com.example.android.travellerchallenge.activity;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.android.travellerchallenge.adapter.ChallengeAdapter;
import com.example.android.travellerchallenge.constant.ProjectConstants;
import com.example.android.travellerchallenge.entity.ChallengeItem;
import com.example.android.travellerchallenge.interfacePackage.GetPlaceCallBack;
import com.example.android.travellerchallenge.util.PlacesLoader;
import com.example.android.travellerchallenge.R;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBufferResponse;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

/**
 * This activity is where place search from Places API resulted
 */

public class SearchResultActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<String> {

    private static final String TAG = "SearchResultActivity";

    private ArrayList<Integer> types = new ArrayList<>();
    private ArrayList<ChallengeItem> challenges = new ArrayList<>();

    private String startPlaceID;

    private LatLng startPlaceLatLng;

    private Location firstActivityLocation;
    private Location startLocation;

    private ChallengeAdapter challengeAdapter;
    private ListView listView;

    Activity context;

    private String REQUEST_URL =  "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feed_list);

        context = this;

        challengeAdapter = new ChallengeAdapter(context, challenges);

        startLocation = new Location("");
        firstActivityLocation = new Location("");

        listView = findViewById(R.id.feed_list);

        Bundle b = getIntent().getExtras();
        if ( b != null){
            REQUEST_URL = b.getString("uri");
            types = b.getIntegerArrayList("types");
            startPlaceLatLng = b.getParcelable("start_point");
        }

        // search from existed challenge
        searchFromExisted();

        // Get a reference to the ConnectivityManager to check state of network connectivity
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connMgr != null){
            // Get details on the currently active default data network
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            // If there is a network connection, fetch data
            if (networkInfo != null && networkInfo.isConnected()) {
                // Get a reference to the LoaderManager, in order to interact with loaders.
                LoaderManager loaderManager = getLoaderManager();

                // Initialize the loader. Pass in the int ID constant defined above and pass in null for
                // the bundle. Pass in this activity for the LoaderCallbacks parameter (which is valid
                // because this activity implements the LoaderCallbacks interface).
                loaderManager.initLoader(ProjectConstants.PLACE_LOADER_ID, null, this);
            }
        }


    }

    @Override
    public Loader<String> onCreateLoader(int id, Bundle args) {
        Uri baseUri = Uri.parse(REQUEST_URL);
        Uri.Builder uriBuilder = baseUri.buildUpon();
        return new PlacesLoader(this, uriBuilder.toString());
    }

    @Override
    public void onLoadFinished(Loader<String> loader, String data) {
        if (data != null && !data.isEmpty()) {
            Log.v(TAG, data);
        }
    }

    @Override
    public void onLoaderReset(Loader<String> loader) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Method to search existed challenges that match the user's preferences, start place and categories
     */
    private void searchFromExisted(){
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        databaseReference.child(ProjectConstants.CHALLENGES_ENTITY).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(final DataSnapshot snapshot : dataSnapshot.getChildren()){
                    final ChallengeItem c = snapshot.getValue(ChallengeItem.class);
                    final ArrayList<Integer> typesList = new ArrayList<>(c.getTypes());
                    startPlaceID = c.getActivities().get(0);
                    getPlaceByPlaceID(new GetPlaceCallBack() {
                        @Override
                        public void OnCallBackGetPlace(Place place) {

                            // creating first activity and start point location variables to calculate distance between them
                            firstActivityLocation.setLongitude(place.getLatLng().longitude);
                            firstActivityLocation.setLatitude(place.getLatLng().latitude);

                            startLocation.setLatitude(startPlaceLatLng.latitude);
                            startLocation.setLongitude(startPlaceLatLng.longitude);

                            float distance = distanceBetweenLocations(startLocation,firstActivityLocation);
                            for(Integer i : typesList){
                                // if distance is less than 1km and challenge contains the selected types by user, add that challenge to adapter
                                if(types.contains(i) && !challenges.contains(c) && distance < 1000){
                                    Log.v(TAG,snapshot.child(ProjectConstants.CHALLENGES_CNAME).getValue(String.class));
                                    challengeAdapter.add(c);
                                }
                            }
                        }
                    });
                }
                // add on click listener to challenge item
                listView.setAdapter(challengeAdapter);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Log.v(TAG, "Challenge item clicked" );
                        Intent intent = new Intent(context,ChallengePageActivity.class);
                        Bundle b = new Bundle();
                        b.putString("id", challengeAdapter.getItem(position).getID());
                        intent.putExtras(b);
                        startActivity(intent);
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    /**
     * This method calculates the distances in meter between user's location and activity POI's location.
     */
    private float distanceBetweenLocations(Location location1, Location location2){
        return location1.distanceTo(location2);
    }

    /**
     * method to get place by place ID using callback
     */
    private void getPlaceByPlaceID (final GetPlaceCallBack getPlaceCallBack){
        GeoDataClient mGeoDataClient = Places.getGeoDataClient(context, null);
        mGeoDataClient.getPlaceById(startPlaceID).addOnCompleteListener(new OnCompleteListener<PlaceBufferResponse>() {
            @Override
            public void onComplete(@NonNull Task<PlaceBufferResponse> task) {
                if (task.isSuccessful()) {
                    // getting first likely place from place buffer
                    PlaceBufferResponse places = task.getResult();
                    Place p = places.get(0);
                    Log.i(TAG, "Place found: " + p.getLatLng());
                    getPlaceCallBack.OnCallBackGetPlace(p);
                    places.release();
                } else {
                    Log.e(TAG, "Place not found.");
                }
            }
        });
    }

}

