package com.example.android.travellerchallenge.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.android.travellerchallenge.R;

import java.util.ArrayList;

import com.example.android.travellerchallenge.constant.ProjectConstants;
import com.example.android.travellerchallenge.entity.ActivityItem;
import com.example.android.travellerchallenge.util.ProjectMethods;

/**
 * This class is array adapter class with for Activity items.
 * That will perform the display of the activity object attributes.
 */
public class ActivityAdapter extends ArrayAdapter<ActivityItem> {

    public ActivityAdapter(Activity context, ArrayList<ActivityItem> activities){
        super(context,0,activities);
    }
    @Override
    @NonNull
    public View getView(int position,View convertView,@NonNull ViewGroup parent) {

        View listItemView = convertView;
        // Check if the existing view is being reused, otherwise inflate the view
        if(listItemView == null){
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.challenge_activity_item,parent,false);
        }

        ActivityItem current = getItem(position);


        if(current != null){
            TextView name = listItemView.findViewById(R.id.activity_name);
            name.setText(current.getName());

            TextView types = listItemView.findViewById(R.id.types);
            String c = ProjectMethods.convertTypesToString(current.getTypes());
            types.setText(c);

            // by default set all status views GONE
            LinearLayout lNotvisited = listItemView.findViewById(R.id.notvisited);
            lNotvisited.setVisibility(View.GONE);
            LinearLayout lOngoing = listItemView.findViewById(R.id.ongoing);
            lOngoing.setVisibility(View.GONE);
            LinearLayout lCompleted = listItemView.findViewById(R.id.completed);
            lCompleted.setVisibility(View.GONE);

            // then set Visible according to status
            switch (current.getStatus()) {
                case ProjectConstants.NOTVISITED:
                    lNotvisited.setVisibility(View.VISIBLE);
                    break;
                case ProjectConstants.ONGOING:
                    lOngoing.setVisibility(View.VISIBLE);
                    break;
                case ProjectConstants.COMPLETED:
                    lCompleted.setVisibility(View.VISIBLE);
                    break;
            }

        }
        return listItemView;
    }
}
