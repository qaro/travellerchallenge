package com.example.android.travellerchallenge.util;

import android.content.AsyncTaskLoader;
import android.content.Context;

/**
 * Loads a list of places by using an AsyncTask to perform the
 * network request to the given URL.
 */

public class PlacesLoader extends AsyncTaskLoader<String> {

    private String mUrl;

    public PlacesLoader(Context context, String url) {
        super(context);
        mUrl = url;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    @Override
    public String loadInBackground() {
        if (mUrl == null) {
            return null;
        }
        // Perform the network request, parse the response, and extract a list of places.
        return QueryUtils.fetchPlacesData(mUrl);
    }
}

