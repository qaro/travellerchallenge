package com.example.android.travellerchallenge.adapter;


import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.android.travellerchallenge.R;

import com.example.android.travellerchallenge.fragment.StepOneFragment;
import com.example.android.travellerchallenge.fragment.StepThreeFragment;
import com.example.android.travellerchallenge.fragment.StepTwoFragment;

/**
 * This class is fragment pager adapter class.
 * That will perform the paging of the step fragment of selection activity.
 */

public class SelectionPageAdapter extends FragmentPagerAdapter{
    private Context mContext;

    public SelectionPageAdapter (Context context, FragmentManager fm){
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new StepOneFragment();
        } else if (position == 1) {
            return new StepTwoFragment();
        } else  {
            return new StepThreeFragment();
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0) {
            return mContext.getString(R.string.step_one);
        } else if (position == 1) {
            return mContext.getString(R.string.step_two);
        } else {
            return mContext.getString(R.string.step_three);
        }

    }

}
