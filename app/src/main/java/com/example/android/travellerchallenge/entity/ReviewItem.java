package com.example.android.travellerchallenge.entity;

public class ReviewItem {

    private String userID;
    private String reviewID;
    private String commentContent;
    private double rate;
    private long timestamp;

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getReviewID() {
        return reviewID;
    }

    public void setReviewID(String reviewID) {
        this.reviewID = reviewID;
    }

    public String getCommentContent() {
        return commentContent;
    }

    public double getRate() {
        return rate;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setCommentContent(String commentContent) {
        this.commentContent = commentContent;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

}
