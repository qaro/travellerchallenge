package com.example.android.travellerchallenge.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.android.travellerchallenge.activity.ChallengePageActivity;
import com.example.android.travellerchallenge.constant.ProjectConstants;
import com.example.android.travellerchallenge.util.CircleTransform;
import com.example.android.travellerchallenge.activity.EditProfileActivity;
import com.example.android.travellerchallenge.activity.FollowInfoActivity;
import com.example.android.travellerchallenge.activity.LoginActivity;
import com.example.android.travellerchallenge.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import com.example.android.travellerchallenge.adapter.ChallengeAdapter;
import com.example.android.travellerchallenge.entity.ChallengeItem;

/**
 * This is the fragment which be loaded to main activity, and display the current user informations.
 * Such as name, city, follower-following count, score, created and completed challenges are displayed.
 * User can perform edit profile and logout operations from this fragment.
 */

public class ProfileFragment extends Fragment {

    public ProfileFragment() {
        // Required empty public constructor
    }

    private ArrayList<ChallengeItem> createdChallenges = new ArrayList<>();
    private ArrayList<ChallengeItem> completedChallenges = new ArrayList<>();

    private ListView listView;
    private TextView usernameView;
    private TextView scoreView, cityView;
    private ImageView profileImage;

    private DatabaseReference databaseReference;

    private Long followerCount;
    private Long followingCount;

    private String userID;

    private Activity context;

    private ChallengeAdapter createdChallengeAdapter;
    private ChallengeAdapter completedChallengeAdapter;

    private static final String TAG = "ProfileFragment";

    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.feed_list, container, false);
        setHasOptionsMenu(true);

        context = getActivity();

        // add user info layout as header to listview
        listView = rootView.findViewById(R.id.feed_list);
        LayoutInflater layoutinflater = getLayoutInflater();
        ViewGroup header = (ViewGroup) layoutinflater.inflate(R.layout.profile_layout, listView, false);
        listView.addHeaderView(header);

        // initialize view
        usernameView = rootView.findViewById(R.id.name_textview);
        scoreView = rootView.findViewById(R.id.score_textview);
        profileImage = rootView.findViewById(R.id.profile_image);
        cityView = rootView.findViewById(R.id.city_textview);


        final FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null){
            userID = currentUser.getUid();
        }
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();

        // initialize adapters
        createdChallengeAdapter = new ChallengeAdapter(context, createdChallenges);
        completedChallengeAdapter = new ChallengeAdapter(context, completedChallenges);
        // listener to get the user info from users child


        DatabaseReference mUsers = databaseReference.child(ProjectConstants.USERS_ENTITY).child(userID);
        mUsers.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {

                    final String username = dataSnapshot.child(ProjectConstants.USERS_USERNAME).getValue(String.class);
                    String score ="0";
                    Long scoreInLong =  dataSnapshot.child(ProjectConstants.USERS_SCORE).getValue(Long.class);
                    if (scoreInLong != null){
                        score = Long.toString(scoreInLong);
                    }

                    String city = dataSnapshot.child(ProjectConstants.USERS_CITY).getValue(String.class);
                    Uri photoUri = Uri.parse(dataSnapshot.child(ProjectConstants.USERS_PHOTO_URI).getValue(String.class));
                    usernameView.setText(username);
                    scoreView.setText(score);
                    cityView.setText(city);
                    Picasso.get().load(photoUri).transform(new CircleTransform()).into(profileImage);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        // listener to get created challenges of the user
        final DatabaseReference created = mUsers.child(ProjectConstants.USERS_CHALLENGES_CREATED);
        created.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    createdChallengeAdapter.clear();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        if(snapshot != null){
                            String challengeID = snapshot.getKey();
                            // listener to get challenge info of all created challenges of the user
                            DatabaseReference challenge = databaseReference.child(ProjectConstants.CHALLENGES_ENTITY).child(challengeID);
                            challenge.addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if(dataSnapshot.exists()) {
                                        ChallengeItem c = dataSnapshot.getValue(ChallengeItem.class);
                                        createdChallengeAdapter.add(c);
                                    }
                                }
                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) { }
        });

        // listener to get completed challenges of the user
        final DatabaseReference completed = databaseReference.child(ProjectConstants.CHALLENGES_FINISHED_ENTITY);
        completed.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(!dataSnapshot.exists()){
                    completed.removeEventListener(this);
                    return;
                }
                completedChallengeAdapter.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    String id = snapshot.child(ProjectConstants.CHALLENGES_FINISHED_USERID).getValue(String.class);
                    String status = snapshot.child(ProjectConstants.CHALLENGES_FINISHED_STATUS).getValue(String.class);
                    // get challenge with the status equals to Completed
                    if(id != null && status != null){
                        if(id.equals(userID) && status.equals(ProjectConstants.CHALLENGE_SUCCESFULL)){
                            String challengeID = snapshot.child(ProjectConstants.CHALLENGES_FINISHED_CHALLENGEID).getValue(String.class);
                            if(challengeID != null){
                                // listener to get challenge info of all created challenges of the user
                                DatabaseReference challenge = databaseReference.child(ProjectConstants.CHALLENGES_ENTITY).child(challengeID);
                                challenge.addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        // add challenge to list
                                        ChallengeItem c = dataSnapshot.getValue(ChallengeItem.class);
                                        completedChallengeAdapter.add(c);
                                    }
                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });
                            }

                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) { }
        });

        // initialize tablayout that contains completed and created challenges
        final TabLayout tabLayout = rootView.findViewById(R.id.tablayout);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                // set adapters accordingly and intent to Challenge page activity by challenge ID
                switch (tab.getPosition()) {
                    case 0:
                        listView.setAdapter(completedChallengeAdapter);
                        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                Log.v(TAG, "Challenge item clicked ");
                                Intent intent = new Intent(context, ChallengePageActivity.class);
                                Bundle b = new Bundle();
                                b.putString("id", completedChallenges.get(position-1).getID());
                                intent.putExtras(b);
                                startActivity(intent);

                            }
                        });
                        break;

                    case 1:
                        listView.setAdapter(createdChallengeAdapter);
                        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                Log.v(TAG, "Challenge item clicked ");
                                Intent intent = new Intent(context, ChallengePageActivity.class);
                                Bundle b = new Bundle();
                                b.putString("id", createdChallenges.get(position-1).getID());
                                intent.putExtras(b);
                                startActivity(intent);

                            }
                        });
                        break;
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        // determining the initial selected tablayout item as completed challenges
        TabLayout.Tab tab = tabLayout.getTabAt(0);
        if(tab != null){
            tab.select();
        }
        listView.setAdapter(completedChallengeAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.v(TAG, "Challenge item clicked ");
                Intent intent = new Intent(context, ChallengePageActivity.class);
                Bundle b = new Bundle();
                b.putString("id", completedChallenges.get(position-1).getID());
                intent.putExtras(b);
                startActivity(intent);
            }
        });

        // set click listener to followers view to intent to Follow Info Page with type Follower
        final TextView followers = rootView.findViewById(R.id.follower_textview);
        LinearLayout followerLayout = rootView.findViewById(R.id.follower_layout);
        followerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, FollowInfoActivity.class);
                Bundle b = new Bundle();
                b.putString("type", ProjectConstants.FOLLOWERS);
                b.putString("id", userID);
                intent.putExtras(b);
                startActivity(intent);
            }
        });
        // set click listener to following view to intent to Follow Info Page with type Following
        final TextView following = rootView.findViewById(R.id.following_textview);
        LinearLayout followingLayout = rootView.findViewById(R.id.following_layout);
        followingLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, FollowInfoActivity.class);
                Bundle b = new Bundle();
                b.putString("type", ProjectConstants.FOLLOWING);
                b.putString("id", userID);
                intent.putExtras(b);
                startActivity(intent);
            }
        });

        // listener to get following and followers count
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                followerCount = dataSnapshot.child(ProjectConstants.FOLLOWERS_ENTITY).child(userID).getChildrenCount();
                followers.setText(String.valueOf(followerCount));
                followingCount = dataSnapshot.child(ProjectConstants.FOLLOWING_ENTITY).child(userID).getChildrenCount();
                following.setText(String.valueOf(followingCount));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.profile_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                // start edit Profile activity
                Intent intentSet = new Intent(context, EditProfileActivity.class);
                startActivity(intentSet);
                break;
            case R.id.action_logout:

                ProgressDialog progressDialog = new ProgressDialog(context);
                progressDialog.setIndeterminate(true);
                progressDialog.setMessage("Logging out...");
                progressDialog.show();

                // perform logout action, and start login activity
                FirebaseAuth.getInstance().signOut();
                Intent intentLogoutActivity = new Intent(context, LoginActivity.class);
                startActivity(intentLogoutActivity);
                break;
        }
        return true;
    }

}
