package com.example.android.travellerchallenge.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.app.Fragment;
import android.widget.TextView;
import com.example.android.travellerchallenge.R;

import java.util.ArrayList;

/**
 * This is the fragment which be loaded to selection activity, and contains review of the user preferences
 * of challenge selection process, such as categories, start end date, start end place.
 */

public class StepThreeFragment extends Fragment  {

    // TextViews
    private TextView origin ;
    private TextView destination;
    private TextView timeInterval;
    private TextView categories;


    // Defining global variables
    private String startPlaceName, endPlaceName;
    private String interval;
    private ArrayList<String> categoryList = new ArrayList<>();

    public StepThreeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull  LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.selection_step3, container, false);

        // Find Textviewa
        origin = rootView.findViewById(R.id.start_point);
        destination = rootView.findViewById(R.id.end_point);
        timeInterval = rootView.findViewById(R.id.time_interval);
        categories = rootView.findViewById(R.id.categories);

        // Update review page
        updateUI();

        return rootView;
    }
    @Override
    public void onResume() {
        // update review page
        updateUI();
        super.onResume();
    }

    private void updateUI(){
        // displaying start and destination place, selected time interval
        origin.setText(startPlaceName);
        destination.setText(endPlaceName);
        timeInterval.setText(interval);
        // Displaying categories
        StringBuilder str = new StringBuilder(500);
        if(!categoryList.isEmpty()){
            for(String s : categoryList){
                str.append(s);
                str.append("\n");
            }
        }
        categories.setText(str.toString());
    }

    /**
     * to update the start and end place names
      */
    public void updatePlace(String startPlaceName, String endPlaceName){
        this.startPlaceName = startPlaceName;
        this.endPlaceName = endPlaceName;
        updateUI();
    }

    /**
     * to update the start and end dates
      */
    public void updateDate(String startDate, String endDate){
        // Setting time interval in this format "start - end"
        interval = startDate + " - " + endDate;
        updateUI();
    }

    /**
     * to update the categories selected by user
      */
    public void updateCategory(ArrayList<String> categories){
        categoryList = categories;
        updateUI();
    }

}