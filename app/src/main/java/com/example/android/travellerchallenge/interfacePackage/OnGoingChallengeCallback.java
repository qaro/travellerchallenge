package com.example.android.travellerchallenge.interfacePackage;

import com.example.android.travellerchallenge.entity.FeedItem;

import java.util.ArrayList;

public interface OnGoingChallengeCallback {

    void onCallbackOnGoingChallenge(ArrayList<FeedItem> feedItemArrayList);

}
