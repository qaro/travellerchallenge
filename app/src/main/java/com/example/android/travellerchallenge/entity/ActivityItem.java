package com.example.android.travellerchallenge.entity;

import java.util.List;

/**
 * This is the Activity Class.
 * Activity is basically the place which has to be visited during the challenge.
 */
public class ActivityItem {
    private String id;
    private int status;
    private String name;
    private List<Integer> types;

    public ActivityItem(){
        this.id = "";
        this.status = 0;
        this.name = "";
        this.types = null;
    }
    public ActivityItem(String id,String name, int status, List<Integer> types){
        this.id = id;
        this.status = status;
        this.name = name;
        this.types = types;
    }

    public String getID(){return this.id;}
    public int getStatus(){return  this.status;}
    public String getName() { return name; }
    public List<Integer> getTypes() { return types; }

    public void setStatus(int s){ this.status = s; }
}


