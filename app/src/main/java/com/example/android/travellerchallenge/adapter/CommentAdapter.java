package com.example.android.travellerchallenge.adapter;

import android.app.Activity;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.android.travellerchallenge.R;
import com.example.android.travellerchallenge.constant.ProjectConstants;
import com.example.android.travellerchallenge.entity.ReviewItem;
import com.example.android.travellerchallenge.util.CircleTransform;
import com.example.android.travellerchallenge.util.ProjectMethods;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * This class is array adapter class with for Review items.
 * That will perform the display of the comment object attributes.
 */

public class CommentAdapter extends ArrayAdapter<ReviewItem> {

    public CommentAdapter(Activity context, ArrayList<ReviewItem> comments){
        super(context,0,comments);
    }

    @Override
    @NonNull
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {

        View listItemView = convertView;
        // Check if the existing view is being reused, otherwise inflate the view
        if(listItemView == null){
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.comment_item,parent,false);
        }

        final ReviewItem current = getItem(position);

        if(current != null){
            // initialize views
            final TextView commentV = listItemView.findViewById(R.id.comment_textview);
            final TextView usernameV = listItemView.findViewById(R.id.username_textview);
            final TextView rateV = listItemView.findViewById(R.id.ratio);
            final ImageView imageV = listItemView.findViewById(R.id.profile_image);
            final TextView dateV = listItemView.findViewById(R.id.created_date);

            // displays attributes of comment and user object
            String userID = current.getUserID();
            DatabaseReference mRef = FirebaseDatabase.getInstance().getReference().child(ProjectConstants.USERS_ENTITY).child(userID);
            mRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    commentV.setText(current.getCommentContent());

                    String username = dataSnapshot.child(ProjectConstants.USERS_USERNAME).getValue(String.class);
                    usernameV.setText(username);

                    rateV.setText(String.valueOf(current.getRate()));

                    dateV.setText(ProjectMethods.convertMillisToDate(current.getTimestamp()));

                    String photoUri = dataSnapshot.child(ProjectConstants.USERS_PHOTO_URI).getValue(String.class);
                    if(photoUri != null) {
                        Picasso.get().load(Uri.parse(photoUri)).transform(new CircleTransform()).into(imageV);
                        imageV.setVisibility(View.VISIBLE);
                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
        return listItemView;
    }

}
