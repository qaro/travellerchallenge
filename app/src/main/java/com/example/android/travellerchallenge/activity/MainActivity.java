package com.example.android.travellerchallenge.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.example.android.travellerchallenge.R;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.FirebaseAuth;

import com.example.android.travellerchallenge.fragment.ChallengeFragment;
import com.example.android.travellerchallenge.fragment.ExploreFragment;
import com.example.android.travellerchallenge.fragment.FeedFragment;
import com.example.android.travellerchallenge.fragment.ProfileFragment;
import com.example.android.travellerchallenge.fragment.SearchFragment;

/**
 * This is the main activity of the application that contains a bottom navigation and 5 fragments
 * such as Feed, Search, Challenge, Explore, Profile fragment.
 * If no logged in user, this activity will intent to login activity.
 */

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        // if no logged in users, intent to login activity
        if (user == null) {
            Intent intentLoginActivity = new Intent(this, LoginActivity.class);
            startActivity(intentLoginActivity);
        }

        // create bottom navigation view
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);

        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        Fragment selectedFragment = null;
                        switch (item.getItemId()) {
                            case R.id.action_home:
                                selectedFragment = new FeedFragment();
                                break;

                            case R.id.action_search:
                                selectedFragment = new SearchFragment();
                                break;

                            case R.id.action_challenge:
                                selectedFragment = new ChallengeFragment();
                                break;

                            case R.id.action_explore:
                                selectedFragment = new ExploreFragment();
                                break;

                            case R.id.action_profile:
                                selectedFragment = new ProfileFragment();
                                break;
                        }
                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.main_container, selectedFragment);
                        transaction.commit();
                        return true;
                    }
                });

        // Set default selected navigation item as Challenge Fragment
        bottomNavigationView.getMenu().getItem(2).setChecked(true);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.main_container, new ChallengeFragment());
        transaction.commit();


    }

}
