package com.example.android.travellerchallenge.fragment;

import android.app.Dialog;
import android.app.DialogFragment;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.android.travellerchallenge.R;
import com.example.android.travellerchallenge.constant.ProjectConstants;
import com.example.android.travellerchallenge.entity.ReviewItem;
import com.example.android.travellerchallenge.util.CircleTransform;
import com.example.android.travellerchallenge.util.ProjectMethods;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;


/**
 * This fragment is a Dialog Fragment which displays the comment.
 * This fragment will be started by clicking a comment item, in order to see challenge info more detailed.
 * The comment ID and the challengeID which the commend belong are retrieved as arguments.
 */

public class ReviewFragment extends DialogFragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.review_dialog_layout, container, false);
        getDialog().setTitle(ProjectConstants.RATE_COMMENT);

        // getting the comment arguments from the intended activity
        String reviewID = getArguments().getString("reviewID");
        String challengeID = getArguments().getString("challengeID");

        if (reviewID != null && challengeID != null){

            final DatabaseReference ref = FirebaseDatabase.getInstance().getReference();

            // database reference to get the comment attributes and display
            DatabaseReference commentsRef = ref.child(ProjectConstants.REVIEWS_ENTITY);
            commentsRef.child(challengeID).child(reviewID).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists()){
                        final ReviewItem currentComment = dataSnapshot.getValue(ReviewItem.class);
                        if(currentComment != null){
                            // db reference to get the info of comment owner user
                            ref.child(ProjectConstants.USERS_ENTITY).child(currentComment.getUserID()).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    // user name
                                    TextView usernameV = rootView.findViewById(R.id.username_textview);
                                    String username = dataSnapshot.child(ProjectConstants.USERS_USERNAME).getValue(String.class);
                                    usernameV.setText(username);

                                    // user image
                                    ImageView imageV = rootView.findViewById(R.id.profile_image);
                                    String photoUri = dataSnapshot.child(ProjectConstants.USERS_PHOTO_URI).getValue(String.class);
                                    if(photoUri != null) {
                                        Picasso.get().load(Uri.parse(photoUri)).transform(new CircleTransform()).into(imageV);
                                        imageV.setVisibility(View.VISIBLE);
                                    }

                                    // comment content
                                    TextView commentV = rootView.findViewById(R.id.comment_textview);
                                    commentV.setText(currentComment.getCommentContent());

                                    // user given rate
                                    TextView rateV = rootView.findViewById(R.id.ratio);
                                    rateV.setText(String.valueOf(currentComment.getRate()));

                                    // comment's creation date
                                    TextView dateV = rootView.findViewById(R.id.created_date);
                                    dateV.setText(ProjectMethods.convertMillisToDate(currentComment.getTimestamp()));
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                        }

                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

        return rootView;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
       return super.onCreateDialog(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();

        Window dialogWindow = getDialog().getWindow();
        if (dialogWindow != null) {
            dialogWindow.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

}
