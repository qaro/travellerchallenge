package com.example.android.travellerchallenge.interfacePackage;

import com.google.android.gms.location.places.Place;

public interface GetPlaceCallBack {

    void OnCallBackGetPlace(Place place);

}
