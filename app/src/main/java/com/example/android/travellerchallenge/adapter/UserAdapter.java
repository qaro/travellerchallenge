package com.example.android.travellerchallenge.adapter;

import android.app.Activity;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.android.travellerchallenge.util.CircleTransform;
import com.example.android.travellerchallenge.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import com.example.android.travellerchallenge.entity.User;

/**
 * Created by Basar Agakadi on 17.03.2018.
 * This class is array adapter class with for user items.
 * That will perform the display of the user object attributes.
 */

public class UserAdapter extends ArrayAdapter<User> {

    public UserAdapter(Activity context, ArrayList<User> users) {
        super(context, 0, users);
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull final ViewGroup parent) {
        View listItemView = convertView;
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.userlist_item, parent, false);
        }

        User user_item = getItem(position);

        if (user_item != null){
            TextView userName = listItemView.findViewById(R.id.username);
            userName.setText(user_item.getUsername());

            TextView city = listItemView.findViewById(R.id.user_city);
            city.setText(user_item.getCity());

            TextView score = listItemView.findViewById(R.id.user_score);
            score.setText(String.valueOf(user_item.getScore()));

            if(user_item.getPhotoUri() != null) {
                ImageView profileImage = listItemView.findViewById(R.id.friend_pi);
                Picasso.get().load(Uri.parse(user_item.getPhotoUri())).transform(new CircleTransform()).into(profileImage);
                profileImage.setVisibility(View.VISIBLE);
            }
        }
        return listItemView;
    }

}
