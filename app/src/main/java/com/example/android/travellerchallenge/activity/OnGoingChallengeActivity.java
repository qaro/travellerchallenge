package com.example.android.travellerchallenge.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.travellerchallenge.R;
import com.example.android.travellerchallenge.constant.ProjectConstants;
import com.example.android.travellerchallenge.fragment.RateCommentFragment;
import com.example.android.travellerchallenge.util.GPSTracker;
import com.example.android.travellerchallenge.util.ProjectMethods;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBufferResponse;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import com.example.android.travellerchallenge.adapter.ActivityAdapter;
import com.example.android.travellerchallenge.entity.ActivityItem;

/**
 * This is the activity to display information of a OngoingChallenge such as name, remaing duration,and activities.
 * All activities and their status will be displayed. User complete activities by clicking listview items
 * If all activities are completed before time's up then the challenge will be deleted from ongoing and appended to completeChallenges
 * The challenge can be canceled from this page by using the cancel button.
 * If the challenge is canceled, it will deleted from the Firebase database's onGoing child, appended to completedChallenges child
 */


public class OnGoingChallengeActivity extends AppCompatActivity {

    private static final String TAG = "OnGoing";

    private Activity context;
    private CountDownTimer counter;
    private ActivityAdapter itemsAdapter;
    private DatabaseReference mDatabase;

    private Long startTime;
    private Integer duration;
    private String userID;
    private Integer challengeScore;
    private Long userScore;

    private Location currentLocation;
    private LatLng onGoingActivityLatLng;

    private boolean locationPermissionGranted = false;
    private boolean isChallengeFinished = false;

    private String  challengeID ;
    private ArrayList<ActivityItem> activities = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feed_list);
        setTitle(ProjectConstants.ON_DUTY);
        context = this;

        // current userID
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if(user != null){
            userID = user.getUid();
        }

        // button to start map activity
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setVisibility(View.VISIBLE);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,MapActivity.class);
                Bundle b = new Bundle();
                b.putString("userID", userID);
                intent.putExtras(b);
                startActivity(intent);
            }
        });

        mDatabase = FirebaseDatabase.getInstance().getReference();

        // adding info layout to the list view as header
        ListView listView = findViewById(R.id.feed_list);
        LayoutInflater layoutinflater = getLayoutInflater();
        ViewGroup header = (ViewGroup) layoutinflater.inflate(R.layout.activity_header, listView, false);
        listView.addHeaderView(header);

        itemsAdapter = new ActivityAdapter(this, activities);
        listView.setAdapter(itemsAdapter);

        // retrieve user current score
        mDatabase.child(ProjectConstants.USERS_ENTITY).child(userID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    userScore = dataSnapshot.child(ProjectConstants.USERS_SCORE).getValue(Long.class);
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        // listener for database Ongoing child reference
        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        final DatabaseReference ref = databaseReference.child(ProjectConstants.ONGOING_ENTITY).child(userID);
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    // get ongoing challenge ID and start time
                    challengeID = dataSnapshot.child(ProjectConstants.ONGOING_CHALLENGE).getValue(String.class);
                    startTime = dataSnapshot.child(ProjectConstants.ONGOING_START_TIME).getValue(Long.class);

                    // listener to get challenge information such as name and duration
                    DatabaseReference cRef = databaseReference.child(ProjectConstants.CHALLENGES_ENTITY).child(challengeID);
                    cRef.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if(dataSnapshot.exists()){
                                // get challenge information name and duration
                                TextView challengeName = findViewById(R.id.challenge_name);
                                challengeName.setText(dataSnapshot.child(ProjectConstants.CHALLENGES_CNAME).getValue(String.class));
                                Integer durationInMin = dataSnapshot.child(ProjectConstants.CHALLENGES_DURATION).getValue(int.class);
                                challengeScore = dataSnapshot.child(ProjectConstants.CHALLENGES_SCORE).getValue(Integer.class);
                                if ( durationInMin != null){
                                    duration = durationInMin*60000;
                                }
                                // compute the remaining time of the challenge, start count down timer
                                Long millisInFuture = duration - (System.currentTimeMillis()-startTime);
                                counter = new CountDownTimer(millisInFuture, 60000) {
                                    TextView durationTV = findViewById(R.id.duration);
                                    public void onTick(long millisUntilFinished) {
                                        // display remaining duration
                                        if(isChallengeFinished){
                                            counter.cancel();
                                        }

                                        String dur;
                                        if (millisUntilFinished / 60000 <= 0){
                                             dur = "Less than 1 min";
                                        } else {
                                            dur = ProjectMethods.convertDurationToString((int) millisUntilFinished / 60000) + " left!";
                                        }
                                        durationTV.setText(dur);
                                    }
                                    public void onFinish() {
                                        durationTV.setText(ProjectConstants.DONE);
                                        if(!isChallengeFinished) {
                                            finishChallenge(ProjectConstants.CHALLENGE_FAILED);
                                        }
                                        AlertDialog.Builder builder = new AlertDialog.Builder(context);

                                        builder.setTitle(ProjectConstants.COUNTDOWN_OVER_TITLE);
                                        builder.setMessage(ProjectConstants.COUNTDOWN_OVER_MESSAGE);
                                        builder.setPositiveButton(ProjectConstants.NEUTRAL_ANSWER, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                context.finish();
                                            }
                                        });
                                        if(!context.isFinishing()) {
                                            builder.show();
                                        }

                                    }
                                };
                                counter.start();
                            }
                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) { }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        // Getting activity status of the ongoing challenge
        ref.child(ProjectConstants.ONGOING_ACTIVITY_STATUS).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    itemsAdapter.clear();
                    for (final DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        // update adapter
                        ActivityItem currentItem = snapshot.getValue(ActivityItem.class);

                        itemsAdapter.add(currentItem);

                     }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) { }
        });

        // set onclick listener to activities, to be used in control process
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long id)
            {
                // update current and next activity status
                int currentPosition = position -1;
                ActivityItem currentActivity = itemsAdapter.getItem(currentPosition);
                // if there is no next activity, indicate as -1
                int nextPosition = -1;
                if(position != activities.size()){
                     nextPosition = position;
                }
                if (currentActivity != null){
                    // enable click if only activity status is ONGOING
                    if(currentActivity.getStatus() == ProjectConstants.ONGOING){
                        // check if device in activty POI
                        checkActivity(currentActivity,currentPosition,nextPosition);
                    } else {
                        Toast.makeText(context,"Only OnGoing Activity can be checked.",Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });

        // set on click listener to cancel button
        Button cancelButton = findViewById(R.id.cancel_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle(ProjectConstants.CHALLENGE_ME);
                builder.setMessage(ProjectConstants.CANCEL_CHALLENGE_MESSAGE);
                builder.setPositiveButton(ProjectConstants.POSITIVE_ANSWER, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // cancel challenge
                        finishChallenge(ProjectConstants.CHALLENGE_CANCELED);
                        context.finish();
                    }
                });
                builder.setNegativeButton(ProjectConstants.NEGATIVE_ANSWER, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) { }
                });
                builder.show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * This is the main method to check user's location permissions and location.
     */
    private void checkActivity(ActivityItem item, final int currentPosition, final int nextPosition) {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Access to the location has not been granted to the app. Prompt the user for permission.
            getLocationPermission();
            Log.i(TAG, "Permission needed");
        } else {
            // Access to the location has been granted to the app. Then get current place of the device
            Log.i(TAG, "Permission granted");
            locationPermissionGranted = true;
            // get current location and check
            getCurrentLocationAndCheck(item, currentPosition, nextPosition);

        }
    }

    /**
     * This method gets the current location and checks whether user is actually visiting that POI or not.
     * using helper service GPSTracker
     * @link GPSTracker
     */
    private void getCurrentLocationAndCheck(final ActivityItem item, final int currentPosition, final int nextPosition){

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Checking...");
        progressDialog.show();

        try{
            // using helper service
            GPSTracker gpsTracker = new GPSTracker(context);

            // until services return the location wait
            Location bufferLocation = null;

            while (bufferLocation == null){
                Log.d(TAG, "Waiting for current location.");
                bufferLocation = gpsTracker.getLocation();
            }

            // then get current location
            currentLocation = bufferLocation;

            // check distance between current location and the activity POI
            checkDistance(item, currentPosition, nextPosition);

            // stop tracking
            gpsTracker.stopUsingGPS();
            progressDialog.dismiss();

        } catch (SecurityException e){
            Log.e(TAG, "getting current location, Security Exception:" + e.getMessage());
            progressDialog.dismiss();
        }


    }

    /**
     * This method gets the distance between 2 locations (user and POI) and updates the status according to it.
     */
    private void checkDistance(ActivityItem item, final int currentPosition, final int nextPosition){
        Log.v(TAG,"Clicked item" + item.getName());

        String placeId = item.getID();

        GeoDataClient geoDataClient = Places.getGeoDataClient(context, null);
        geoDataClient.getPlaceById(placeId).addOnCompleteListener(new OnCompleteListener<PlaceBufferResponse>() {
            @Override
            public void onComplete(@NonNull Task<PlaceBufferResponse> task) {
                if(task.isSuccessful()){

                    // get POI of the activity
                    PlaceBufferResponse places = task.getResult();
                    Place place = places.get(0);
                    onGoingActivityLatLng = place.getLatLng();

                    // create the location o POI
                    Location activityLocation = new Location("activityLocation");
                    activityLocation.setLatitude(onGoingActivityLatLng.latitude);
                    activityLocation.setLongitude(onGoingActivityLatLng.longitude);

                    // compare the distance
                    if(distanceBetweenUserAndActivityPOI(currentLocation, activityLocation) <= ProjectConstants.DISTANCE_MAX){
                        updateStatus(currentPosition, nextPosition);
                    }
                    else{
                        Toast.makeText(context, "You are not in this location!", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });

    }

    /**
     * This method gets permission for location information of the user if he/she did not give permission before.
     */
    private void getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        Log.i(TAG, "getLocationPermission");
        if (ContextCompat.checkSelfPermission(context.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            locationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(context,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    ProjectConstants.PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            Log.i(TAG, "getLocationPermission:" + locationPermissionGranted);
        }
    }

    /**
     * This method calculates the distances in meter between user's location and activity POI's location.
     */
    private float distanceBetweenUserAndActivityPOI(Location userLocation, Location activityLocation){
        return userLocation.distanceTo(activityLocation);
    }

    // method to update current clicked and next activity status
    private void updateStatus(int currentPosition, int nextPosition){
        // current activity status will be completed, next activity status will be ongoing
        DatabaseReference ref = mDatabase.child(ProjectConstants.ONGOING_ENTITY).child(userID).child(ProjectConstants.ONGOING_ACTIVITY_STATUS);
        ref.child(String.valueOf(currentPosition)).child(ProjectConstants.ONGOING_STATUS).setValue(ProjectConstants.COMPLETED);
        if(nextPosition != -1){
            ref.child(String.valueOf(nextPosition)).child(ProjectConstants.ONGOING_STATUS).setValue(ProjectConstants.ONGOING);
        }
        if(currentPosition == activities.size() - 1){
            finishChallenge(ProjectConstants.CHALLENGE_SUCCESFULL);

            updateUserScore(); // Updates the user's score on firebase table.

            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(ProjectConstants.CHALLENGE_SUCCESFULLY_DONE_TITLE);
            builder.setMessage(ProjectConstants.CHALLENGE_SUCCESFULLY_DONE_MESSAGE);
            builder.setPositiveButton(ProjectConstants.NEUTRAL_ANSWER, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    FragmentManager fm = getFragmentManager();
                    RateCommentFragment dialogFragment = new RateCommentFragment();

                    Bundle args = new Bundle();
                    args.putString("challengeID", challengeID);
                    dialogFragment.setArguments(args);
                    dialogFragment.setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomDialog);
                    dialogFragment.show(fm, "RateComment Fragment");
                }
            });
            builder.show();
        }
    }

    /**
     * This method is used to update the user's score after completing a challenge.
     */
    private void updateUserScore() {
        mDatabase.child(ProjectConstants.USERS_ENTITY).child(userID).child("score").setValue(challengeScore + userScore);
    }

    /**
     * method to call when ongoing challenge canceled.
     * Ongoing challenge will be deleted from Ongoing child then appended to challengesCompleted child
      */
    private void finishChallenge(String status){
        isChallengeFinished = true;
        mDatabase.child(ProjectConstants.ONGOING_ENTITY).child(userID).removeValue();
        DatabaseReference finishedChallenges = mDatabase.child(ProjectConstants.CHALLENGES_FINISHED_ENTITY);
        String completionID = finishedChallenges.push().getKey();
        finishedChallenges.child(completionID).child(ProjectConstants.CHALLENGES_FINISHED_CHALLENGEID).setValue(challengeID);
        finishedChallenges.child(completionID).child(ProjectConstants.CHALLENGES_FINISHED_USERID).setValue(userID);
        finishedChallenges.child(completionID).child(ProjectConstants.CHALLENGES_FINISHED_STATUS).setValue(status);
        finishedChallenges.child(completionID).child(ProjectConstants.CHALLENGES_FINISHED_TIMESTAMP).setValue(System.currentTimeMillis());
    }
}