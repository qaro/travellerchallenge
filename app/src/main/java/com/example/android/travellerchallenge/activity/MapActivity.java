package com.example.android.travellerchallenge.activity;


import com.example.android.travellerchallenge.constant.ProjectConstants;
import com.example.android.travellerchallenge.util.PermissionUtils;
import com.example.android.travellerchallenge.R;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBufferResponse;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;


import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


/**
 * This demo shows how GMS Location can be used to check for changes to the users location.  The
 * "My Location" button uses GMS Location to set the blue dot representing the users location.
 * Permission for {@link android.Manifest.permission#ACCESS_FINE_LOCATION} is requested at run
 * time. If the permission has not been granted, the Activity is finished with an error message.
 */
public class MapActivity extends AppCompatActivity
        implements
        OnMyLocationButtonClickListener,
        OnMyLocationClickListener,
        OnMapReadyCallback,
        ActivityCompat.OnRequestPermissionsResultCallback {

    /**
     * Request code for location permission request.
     *
     * @see #onRequestPermissionsResult(int, String[], int[])
     */
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;

    /**
     * Flag indicating whether a requested permission has been denied after returning in
     * {@link #onRequestPermissionsResult(int, String[], int[])}.
     */
    private boolean mPermissionDenied = false;

    private static final String TAG = "MapActivity";

    private GoogleMap mMap;
    private String userID;
    private String challengeID;
    private Activity context;
    private Integer status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_layout);

        context =this;

        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Bundle b = this.getIntent().getExtras();
        if(b != null){
            userID = b.getString("userID");
            challengeID = b.getString("challengeID");
        }

        final LatLngBounds.Builder builder = new LatLngBounds.Builder();

        // to display ongoing challenge activities on map
        if (userID != null){
            DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
            mDatabase.child(ProjectConstants.ONGOING_ENTITY).child(userID).child(ProjectConstants.ONGOING_ACTIVITY_STATUS)
                    .addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if(dataSnapshot.exists()) {
                                for (final DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                    String PlaceID = snapshot.child("id").getValue(String.class);
                                    GeoDataClient mGeoDataClient = Places.getGeoDataClient(context, null);
                                    mGeoDataClient.getPlaceById(PlaceID).addOnCompleteListener(new OnCompleteListener<PlaceBufferResponse>() {
                                        @Override
                                        public void onComplete(@NonNull Task<PlaceBufferResponse> task) {
                                            if (task.isSuccessful()) {
                                                // getting first likely place from place buffer
                                                PlaceBufferResponse places = task.getResult();
                                                Place p = places.get(0);
                                                Log.i(TAG, "Place found: " + p.getLatLng());

                                                status = snapshot.child("status").getValue(Integer.class);

                                                Marker m = mMap.addMarker(new MarkerOptions().position(p.getLatLng())
                                                        .title(p.getName().toString())
                                                        .icon(BitmapDescriptorFactory.defaultMarker(setColor(status))));

                                                // to configure bounds
                                                builder.include(m.getPosition());
                                                LatLngBounds bounds = builder.build();
                                                int padding = 200; // offset from edges of the map in pixels
                                                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                                                mMap.moveCamera(cu);

                                                places.release();
                                            } else {
                                                Log.e(TAG, "Place not found.");
                                            }
                                        }
                                    });
                                }
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
        }

        // to display activities of a challenge on the map
        else if (challengeID != null){
            DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
            mDatabase.child(ProjectConstants.CHALLENGES_ENTITY).child(challengeID).child(ProjectConstants.CHALLENGES_ACTIVITIES)
                    .addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if(dataSnapshot.exists()) {
                                for (final DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                    String PlaceID = snapshot.getValue(String.class);
                                    GeoDataClient mGeoDataClient = Places.getGeoDataClient(context, null);
                                    mGeoDataClient.getPlaceById(PlaceID).addOnCompleteListener(new OnCompleteListener<PlaceBufferResponse>() {
                                        @Override
                                        public void onComplete(@NonNull Task<PlaceBufferResponse> task) {
                                            if (task.isSuccessful()) {
                                                // getting first likely place from place buffer
                                                PlaceBufferResponse places = task.getResult();
                                                Place p = places.get(0);
                                                Log.i(TAG, "Place found: " + p.getLatLng());

                                                Marker m = mMap.addMarker(new MarkerOptions().position(p.getLatLng())
                                                        .title(p.getName().toString()));

                                                // to configure bounds
                                                builder.include(m.getPosition());
                                                LatLngBounds bounds = builder.build();
                                                int padding = 200; // offset from edges of the map in pixels
                                                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                                                mMap.moveCamera(cu);

                                                places.release();
                                            } else {
                                                Log.e(TAG, "Place not found.");
                                            }
                                        }
                                    });
                                }
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
        }



    }

    @Override
    public void onMapReady(GoogleMap map) {
        mMap = map;

        mMap.setOnMyLocationButtonClickListener(this);
        mMap.setOnMyLocationClickListener(this);
        enableMyLocation();

    }

    /**
     * Enables the My Location layer if the fine location permission has been granted.
     */
    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        } else if (mMap != null) {
            // Access to the location has been granted to the app.
            mMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public boolean onMyLocationButtonClick() {
        Toast.makeText(this, "MyLocation button clicked", Toast.LENGTH_SHORT).show();
        // Return false so that we don't consume the event and the default behavior still occurs
        // (the camera animates to the user's current position).
        return false;
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {
        Toast.makeText(this, "Current location:\n" + location, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
            return;
        }

        if (PermissionUtils.isPermissionGranted(permissions, grantResults,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Enable the my location layer if the permission has been granted.
            enableMyLocation();
        } else {
            // Display the missing permission error dialog when the com.example.android.travellerchallenge.fragments resume.
            mPermissionDenied = true;
        }
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        if (mPermissionDenied) {
            // Permission was not granted, display error dialog.
            showMissingPermissionError();
            mPermissionDenied = false;
        }
    }

    /**
     * Displays a dialog with error message explaining that the location permission is missing.
     */
    private void showMissingPermissionError() {
        PermissionUtils.PermissionDeniedDialog
                .newInstance(true).show(getSupportFragmentManager(), "dialog");
    }

    /**
     * change the color of the marker according to status
     */
    private float setColor(Integer status){
        float color = BitmapDescriptorFactory.HUE_RED;
        switch (status){
            case 1:
                color = BitmapDescriptorFactory.HUE_RED;
                Log.v(TAG,"red");
                break;
            case 2:
                color = BitmapDescriptorFactory.HUE_ORANGE;
                break;
            case 3:
                color = BitmapDescriptorFactory.HUE_GREEN;
                break;
        }
        return color;
    }






}

