package com.example.android.travellerchallenge.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.travellerchallenge.constant.ProjectConstants;
import com.example.android.travellerchallenge.util.CircleTransform;
import com.example.android.travellerchallenge.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import com.example.android.travellerchallenge.adapter.ChallengeAdapter;

import com.example.android.travellerchallenge.entity.ChallengeItem;

/**
 * This activity is used to display information of other users. Current user can Follow and Unfollow displayed user.
 * Current user can intent to FollowInfoActivity where Follower or Following list is displayed, by intent.
 * In the activity, name, city, follower-following count, score, created and completed challenges are displayed.
 */

public class UserProfileActivity extends AppCompatActivity {

    private static final String TAG = "UserProfileActivity";

    private ArrayList<ChallengeItem> createdChallenges=  new ArrayList<>();
    private ArrayList<ChallengeItem> completedChallenges=  new ArrayList<>();

    private ListView listView;
    private TextView usernameView;
    private TextView scoreView, cityView;
    private ImageView profileImage;

    private Long followerCount;
    private Long followingCount;

    private String userID;
    private String currentUserID;

    private final FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

    private DatabaseReference databaseReference;
    private Activity context;

    private ChallengeAdapter createdChallengeAdapter;
    private ChallengeAdapter completedChallengeAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feed_list);

        context = this;

        Bundle b = this.getIntent().getExtras();
        if(b != null){
            userID = b.getString("userid");
        }

        // add user info layout as header to listview
        listView = findViewById(R.id.feed_list);
        LayoutInflater layoutinflater = getLayoutInflater();
        ViewGroup header = (ViewGroup) layoutinflater.inflate(R.layout.profile_layout, listView, false);
        listView.addHeaderView(header);

        // initialize view
        usernameView = findViewById(R.id.name_textview);
        scoreView = findViewById(R.id.score_textview);
        profileImage = findViewById(R.id.profile_image);
        cityView = findViewById(R.id.city_textview);

        // initialize adapters
        createdChallengeAdapter = new ChallengeAdapter(context, createdChallenges);
        completedChallengeAdapter = new ChallengeAdapter(context, completedChallenges);

        if(currentUser != null){
            currentUserID = currentUser.getUid();
        }

        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();

        DatabaseReference mUsers = databaseReference.child(ProjectConstants.USERS_ENTITY).child(userID);

        // listener to get the user info from users child
        mUsers.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.getKey().equals(userID)) {
                    String usernameToShow = dataSnapshot.child(ProjectConstants.USERS_USERNAME).getValue(String.class);
                    String score ="0";
                    Long scoreInLong =  dataSnapshot.child(ProjectConstants.USERS_SCORE).getValue(Long.class);
                    if (scoreInLong != null){
                        score = Long.toString(scoreInLong);
                    }
                    String city = dataSnapshot.child(ProjectConstants.USERS_CITY).getValue(String.class);
                    Uri photoUri = Uri.parse(dataSnapshot.child(ProjectConstants.USERS_PHOTO_URI).getValue(String.class));
                    usernameView.setText(usernameToShow);
                    scoreView.setText(score);
                    cityView.setText(city);
                    Picasso.get().load(photoUri).transform(new CircleTransform()).into(profileImage);
                }
            }


            @Override
            public void onCancelled(DatabaseError databaseError) { }
        });

        // listener to get created challenges of the user
        DatabaseReference created = databaseReference.child(ProjectConstants.USERS_ENTITY).child(userID).child(ProjectConstants.USERS_CHALLENGES_CREATED);
        created.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    createdChallengeAdapter.clear();
                    for(DataSnapshot snapshot: dataSnapshot.getChildren()){
                        String challengeID = snapshot.getKey();
                        // listener to get challenge info of all created challenges of the user
                        DatabaseReference challenge = databaseReference.child(ProjectConstants.CHALLENGES_ENTITY).child(challengeID);
                        challenge.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                // add challenge to list
                                ChallengeItem c = dataSnapshot.getValue(ChallengeItem.class);
                                createdChallengeAdapter.add(c);
                            }
                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                            }
                        });
                    }
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        // listener to get completed challenges of the user
        DatabaseReference completed = databaseReference.child(ProjectConstants.CHALLENGES_FINISHED_ENTITY);
        completed.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    completedChallengeAdapter.clear();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        String id = snapshot.child(ProjectConstants.CHALLENGES_FINISHED_USERID).getValue(String.class);
                        String status = snapshot.child(ProjectConstants.CHALLENGES_FINISHED_STATUS).getValue(String.class);
                        if(id != null && status != null) {
                            // get challenge with the status equals to Completed
                            if (id.equals(userID) && status.equals(ProjectConstants.CHALLENGE_SUCCESFULL)) {
                                String challengeID = snapshot.child(ProjectConstants.CHALLENGES_FINISHED_CHALLENGEID).getValue(String.class);

                                if(challengeID != null){
                                    // listener to get challenge info of all created challenges of the user
                                    DatabaseReference challenge = databaseReference.child(ProjectConstants.CHALLENGES_ENTITY).child(challengeID);
                                    challenge.addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            // add challenge to list
                                            ChallengeItem c = dataSnapshot.getValue(ChallengeItem.class);
                                            completedChallengeAdapter.add(c);
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {

                                        }
                                    });
                                }
                            }
                        }
                    }
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) { }
        });

        // initialize tablayout that contains completed and created challenges
        final TabLayout tabLayout = findViewById(R.id.tablayout);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                // set adapters accordingly and intent to Challenge page activity by challenge ID
                switch (tab.getPosition()) {
                    case 0:
                        listView.setAdapter(completedChallengeAdapter);
                        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                Log.v(TAG, "Challenge item clicked ");

                                Intent intent = new Intent(context, ChallengePageActivity.class);
                                Bundle b = new Bundle();
                                b.putString("id", completedChallenges.get(position-1).getID());

                                intent.putExtras(b);
                                startActivity(intent);

                            }
                        });
                        break;

                    case 1:
                        listView.setAdapter(createdChallengeAdapter);
                        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                Log.v(TAG, "Challenge item clicked ");

                                Intent intent = new Intent(context, ChallengePageActivity.class);
                                Bundle b = new Bundle();
                                b.putString("id", createdChallenges.get(position-1).getID());
                                intent.putExtras(b);
                                startActivity(intent);

                            }
                        });
                        break;
                }

            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) { }

            @Override
            public void onTabReselected(TabLayout.Tab tab) { }
        });

        // determining the initial selected tablayout item as completed challenges
        TabLayout.Tab tab = tabLayout.getTabAt(0);
        if(tab != null){
            tab.select();
        }

        // add click listener
        listView.setAdapter(completedChallengeAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.v(TAG, "Challenge item clicked ");
                Intent intent = new Intent(context, ChallengePageActivity.class);
                Bundle b = new Bundle();
                b.putString("id", completedChallenges.get(position-1).getID());
                intent.putExtras(b);
                startActivity(intent);
            }
        });

        // set click listener to followers view to intent to Follow Info Page with type Follower
        final TextView followers = findViewById(R.id.follower_textview);
        LinearLayout followerLayout = findViewById(R.id.follower_layout);
        followerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, FollowInfoActivity.class);
                Bundle b = new Bundle();
                b.putString("type", ProjectConstants.FOLLOWERS);
                b.putString("id", userID);
                intent.putExtras(b);
                startActivity(intent);
            }
        });
        // set click listener to following view to intent to Follow Info Page with type Following
        final TextView following = findViewById(R.id.following_textview);
        LinearLayout followingLayout = findViewById(R.id.following_layout);
        followingLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, FollowInfoActivity.class);
                Bundle b = new Bundle();
                b.putString("type", ProjectConstants.FOLLOWING);
                b.putString("id", userID);
                intent.putExtras(b);
                startActivity(intent);
            }
        });

        // listener to get following and followers count
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot != null){
                    followerCount = dataSnapshot.child(ProjectConstants.FOLLOWERS_ENTITY).child(userID).getChildrenCount();
                    followers.setText(String.valueOf(followerCount));
                    followingCount = dataSnapshot.child(ProjectConstants.FOLLOWING_ENTITY).child(userID).getChildrenCount();
                    following.setText(String.valueOf(followingCount));
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Bundle b = this.getIntent().getExtras();

        switch (item.getItemId()) {
            case R.id.action_follow:
                // perform follow user action
                if(b != null){
                    followUser(b.getString("userid"));
                }

                break;
            case R.id.action_unfollow:
                // perform unfollow user action
                if(b != null){
                    unfollowUser(b.getString("userid"));
                }
                break;
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.user_profile_menu, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        final MenuItem unfollow = menu.findItem(R.id.action_unfollow);
        final MenuItem follow = menu.findItem(R.id.action_follow);

        // update menu item by follow info of the user
        databaseReference = FirebaseDatabase.getInstance().getReference();
        databaseReference.child(ProjectConstants.FOLLOWING_ENTITY).child(currentUserID).child(userID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    // if user is already followed
                    unfollow.setVisible(true);
                    follow.setVisible(false);
                }
                else{
                    // if user is not followed
                    follow.setVisible(true);
                    unfollow.setVisible(false);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        return super.onPrepareOptionsMenu(menu);
    }

    /**
     * method to follow user
      */
    public void followUser(String userID){

        DatabaseReference followerData = databaseReference.child(ProjectConstants.FOLLOWERS_ENTITY);
        DatabaseReference followingData = databaseReference.child(ProjectConstants.FOLLOWING_ENTITY);

        // add currentUser to followerData as users child
        followerData.child(userID).child(currentUserID).setValue("follower").addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Toast toast = Toast.makeText(UserProfileActivity.this, ProjectConstants.FOLLOW_SUCCESSFUL, Toast.LENGTH_LONG);
                toast.show();
            }
        });

        // add user to followerData as currentUser child
        followingData.child(currentUserID).child(userID).setValue("following").addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

            }
        });

    }

    /**
     * method to unfollow user
      */
    public void unfollowUser(String userID){
        databaseReference = FirebaseDatabase.getInstance().getReference();

        // delete user from both Following and Follower Data in database
        databaseReference.child(ProjectConstants.FOLLOWING_ENTITY).child(currentUserID).child(userID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                dataSnapshot.getRef().removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Toast toast = Toast.makeText(UserProfileActivity.this, ProjectConstants.UNFOLLOW_SUCCESSFUL, Toast.LENGTH_LONG);
                        toast.show();
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        databaseReference.child(ProjectConstants.FOLLOWERS_ENTITY).child(userID).child(currentUserID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                dataSnapshot.getRef().removeValue();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

}