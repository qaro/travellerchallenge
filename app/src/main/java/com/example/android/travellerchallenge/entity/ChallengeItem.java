package com.example.android.travellerchallenge.entity;


import java.util.ArrayList;
import java.util.List;

/**
 * This is the Challenge Class.
 * Challenge is basically the task which contains multiple activities
 * Challenge has to be completed during the given time.
 * If successfully completed, user gain the points as much as the challenge score
 */

public class ChallengeItem {

    private String id;
    private String cname;
    private String creatorID;
    private String city;
    private double rate;
    private int duration;
    private int score;
    private int rateCount;
    private int commentCount;
    private ArrayList<String> activities;
    private List<Integer> types;
    private long date;

    public ChallengeItem(){}

    public ChallengeItem (String challengeID, String name, String creatorid, String city, double rate,
                          int duration, int score, int rateCount, int commentCount, ArrayList<String> activities,List<Integer> types,long createdDate ){
        this.id = challengeID;
        cname = name;
        this.creatorID = creatorid;
        this.city = city;
        this.rate = rate;
        this.duration = duration;
        this.score =score;
        this.rateCount = rateCount;
        this.commentCount= commentCount;
        this.activities = activities;
        this.types=types;
        this.date = createdDate;
    }

    public String getID (){return id;}
    public String getCName (){return cname;}
    public String getCreatorID (){return creatorID;}
    public String getCity (){return city;}
    public double getRate (){return rate;}
    public int getDuration (){return duration;}
    public int getScore (){return score;}
    public int getRateCount() { return rateCount; }
    public int getCommentCount (){return commentCount;}
    public ArrayList<String> getActivities() { return activities; }
    public List<Integer> getTypes() { return types; }
    public long getDate() { return date; }
}
