package com.example.android.travellerchallenge.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.travellerchallenge.R;
import com.example.android.travellerchallenge.constant.ProjectConstants;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import com.example.android.travellerchallenge.entity.User;

/**
 * This is the activity to login the system. The user can use Firebase or Google authentication to login.
 * The sing up activity can be start from this activity by intent.
 * If login success, main activity will be started by intent.
 * If Google Authentication selected, user will be appended to database with given parameters.
 */

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";

    private Button loginButton;
    private TextView loginEmail;
    private TextView loginPassword;

    private FirebaseAuth uAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private GoogleSignInClient mGoogleSignInClient;

    private final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();

    @Override
    protected void onStart() {
        super.onStart();
        uAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);

        // initialize views
        loginEmail = findViewById(R.id.login_email);
        loginPassword = findViewById(R.id.login_password);
        loginButton = findViewById(R.id.login_button);

        // initialize authentication
        uAuth = FirebaseAuth.getInstance();

        SignInButton mGoogleSignInButton = findViewById(R.id.sign_in_google);

        // add click listener to google sign in button
        mGoogleSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this);
                progressDialog.setIndeterminate(true);
                progressDialog.setMessage("Authenticating...");
                progressDialog.show();
                // google sign in action
                signIn();
            }
        });

        // authentication listener
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                // if current user is already exist, go directly to main activity
                // no authentication is needed
                if(FirebaseAuth.getInstance().getCurrentUser() != null){
                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                }

            }
        };

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        // set click listener to login button
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // perform login action
                loginAction();
            }
        });

        // Navigate to sing up page
        TextView linkSignup = findViewById(R.id.link_signup);
        linkSignup.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent signupIntent = new Intent(LoginActivity.this,SignupActivity.class);
                startActivityForResult(signupIntent, ProjectConstants.REQUEST_SIGNUP);
            }
        });

    }

    /**
     * method to call to sign in with google, start activity for RC_SIGN_IN
      */
    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, ProjectConstants.RC_SIGN_IN);
    }

    /**
     * method to get request result
      */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == ProjectConstants.RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with google
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e);
                // ...
            }
        }
        // Result returned from launching the Intent sign up activity
        else if (requestCode == ProjectConstants.REQUEST_SIGNUP){
            if (resultCode == RESULT_OK) {
                loginSuccessToast();
            }
        }
    }

    /**
     * method to call to authenticate with Google
      */
    private void firebaseAuthWithGoogle(GoogleSignInAccount account) {
        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        uAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull final Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");

                            final FirebaseUser firebaseUser = uAuth.getCurrentUser();

                            databaseReference.child(ProjectConstants.USERS_ENTITY).addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if(firebaseUser != null){
                                        if(!dataSnapshot.child(firebaseUser.getUid()).exists()){
                                            if(firebaseUser.getPhotoUrl() != null && firebaseUser.getEmail() != null ) {
                                                String username = firebaseUser.getEmail().split("@",2)[0];
                                                String email = firebaseUser.getEmail();
                                                long score = 0;
                                                String photoUriString = firebaseUser.getPhotoUrl().toString();
                                                User user = new User(firebaseUser.getUid(), username, email, score, "", photoUriString);
                                                appendUserWithGoogleInfo(firebaseUser.getUid(), user);
                                                Log.w(TAG, "New user with uid: " + firebaseUser.getUid() + " appended.", task.getException());
                                            }
                                        }
                                        else{
                                            Log.w(TAG, "User with uid: " + firebaseUser.getUid() + " already exists. Nothing extra is done to append user.", task.getException());
                                        }
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                        } else {
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                        }
                    }
                });
    }

    /**
     * if google sign in, append user with these parameters to database
      */
    private void appendUserWithGoogleInfo(String userId, User user) {
        DatabaseReference mDatabase;
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child(ProjectConstants.USERS_ENTITY).child(userId).setValue(user);
    }

    @Override
    public void onBackPressed() {
        // Disable going back to the MainActivity
        moveTaskToBack(true);
    }

    /**
     * method to perform login action
      */
    private void loginAction () {
        // Checking that user input are valid
        if(!validation()){
            loginFailedToast();
            return;
        }
        // disable the login button to avoid multiple login attempts
        loginButton.setEnabled(false);

        //Creating dialog box
        final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Authenticating...");
        progressDialog.show();

        // getting parameters to login from UI
        String emailText = loginEmail.getText().toString();
        String passwordText = loginPassword.getText().toString();

        // task to loging
        uAuth.signInWithEmailAndPassword(emailText, passwordText).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    // Login successful
                    loginSuccessToast();
                }
                else{
                    // Login failed
                    progressDialog.dismiss();
                    loginFailedToast();
                }
            }
        });

    }

    /**
     * method to inform user with login result and intent to main activity
      */
    private void loginSuccessToast(){
        Toast toast = Toast.makeText(this, ProjectConstants.LOGIN_SUCCESSFUL , Toast.LENGTH_LONG);
        toast.show();
        loginButton.setEnabled(true);
        Intent mainIntent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(mainIntent);
    }

    /**
     * method to inform user with login result
      */
    private void loginFailedToast(){
        Toast toast = Toast.makeText(this, ProjectConstants.LOGIN_FAILED , Toast.LENGTH_LONG);
        toast.show();
        loginButton.setEnabled(true);
    }

    /**
     * method to validate user input
      */
    private boolean validation(){

        boolean validInput = true;

        String emailText = loginEmail.getText().toString();
        String passwordText = loginPassword.getText().toString();

        // email is required in mail format
        if (emailText.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(emailText).matches()) {
            loginEmail.setError(ProjectConstants.EMAIL_REQUIRED);
            validInput=false;
        } else {
            loginEmail.setError(null);
        }
        // password is required and must be larger than 6
        if (passwordText.isEmpty() || passwordText.length() < 6) {
            loginPassword.setError(ProjectConstants.PASSWORD_REQUIRED);
            validInput=false;

        } else {
            loginPassword.setError(null);
        }
        return validInput;
    }
}
