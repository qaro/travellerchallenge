package com.example.android.travellerchallenge.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.design.widget.FloatingActionButton;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.android.travellerchallenge.activity.OnGoingChallengeActivity;
import com.example.android.travellerchallenge.adapter.ChallengeAdapter;
import com.example.android.travellerchallenge.constant.ProjectConstants;
import com.example.android.travellerchallenge.entity.ChallengeItem;
import com.example.android.travellerchallenge.activity.ChallengePageActivity;
import com.example.android.travellerchallenge.activity.CreateChallengeActivity;
import com.example.android.travellerchallenge.R;
import com.example.android.travellerchallenge.activity.SelectionActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;

/**
 * This is the fragment which be loaded to main activity, and display the recent and popular challenges.
 * User can perform create and search challenge operations from this fragment.
 * If user has Ongoing challenge, it will also displayed in this fragment by intent to OnGoingChallengeActivity.
 */

public class ChallengeFragment extends Fragment {

    private ListView listView;

    private ArrayList<ChallengeItem> recent = new ArrayList<>();
    private ArrayList<ChallengeItem> popular = new ArrayList<>();

    private ChallengeAdapter recentChallengeAdapter;
    private ChallengeAdapter popularChallengeAdapter;

    private static final String TAG = "ChallengeFragment";


    public ChallengeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.challange_layout, container, false);
        setHasOptionsMenu(true);

        // initialize adapters for recent and popular list views
        recentChallengeAdapter = new ChallengeAdapter(getActivity(), recent);
        popularChallengeAdapter = new ChallengeAdapter(getActivity(), popular);

        listView = rootView.findViewById(R.id.list_view);

        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        final DatabaseReference databaseReference = firebaseDatabase.getReference();

        // floating action button to intent whether the ongoing or selection activity
        FloatingActionButton fab = rootView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                if(user != null){
                    final String userID = user.getUid();

                    // listener to check if user has Ongoing activity
                    DatabaseReference rootRef = databaseReference.child(ProjectConstants.ONGOING_ENTITY);
                    rootRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            // current user has Ongoing Challenge, then intent to OnGoingChallengeActivity
                            if (dataSnapshot.hasChild(userID)) {
                                Intent intent = new Intent(getActivity(),OnGoingChallengeActivity.class);
                                startActivity(intent);
                            } else{
                                // else intent to SelectionActivity
                                Intent intent = new Intent(getActivity(),SelectionActivity.class);
                                startActivity(intent);
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
            }
        });

        // listener to get challenges ordered by created DATE
        DatabaseReference challenge = databaseReference.child(ProjectConstants.CHALLENGES_ENTITY);
        challenge.orderByChild(ProjectConstants.CHALLENGES_DATE).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    recentChallengeAdapter.clear();
                    // add all challenges to recentChallengeAdapter to display
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        ChallengeItem c = snapshot.getValue(ChallengeItem.class);
                        recentChallengeAdapter.add(c);
                    }
                }

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        // listener to get challenges ordered by RATE
        challenge.orderByChild(ProjectConstants.CHALLENGES_RATE).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    popularChallengeAdapter.clear();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        Integer r = snapshot.child(ProjectConstants.CHALLENGES_RATE).getValue(int.class);
                        if( r != null && r != 0) {
                            // add all challenges to popularChallengeAdapter to display
                            ChallengeItem c = snapshot.getValue(ChallengeItem.class);
                            popularChallengeAdapter.add(c);
                        }
                    }
                    // display DESC order
                    Collections.reverse(popular);
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        // initialize tablayout that contains recent and popular tabs
        final TabLayout tabLayout = rootView.findViewById(R.id.tablayout);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                // set adapter according to tab selected, add click listener
                switch (tab.getPosition()) {
                    case 0:
                        // recent challenges
                        listView.setAdapter(recentChallengeAdapter);
                        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                Log.v(TAG, "Challenge item clicked" );
                                Intent intent = new Intent(getActivity(),ChallengePageActivity.class);
                                Bundle b = new Bundle();
                                b.putString("id", recentChallengeAdapter.getItem(position).getID());
                                intent.putExtras(b);
                                startActivity(intent);
                            }
                        });
                        break;

                    case 1:
                        // popular challenges
                        listView.setAdapter(popularChallengeAdapter);
                        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                Log.v(TAG, "Challenge item clicked" );
                                Intent intent = new Intent(getActivity(),ChallengePageActivity.class);
                                Bundle b = new Bundle();
                                b.putString("id", popularChallengeAdapter.getItem(position).getID());
                                intent.putExtras(b);
                                startActivity(intent);
                            }
                        });
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        // determining the initial selected tablayout item as recent challenge list
        TabLayout.Tab tab = tabLayout.getTabAt(0);
        if(tab != null){
            tab.select();
        }
        listView.setAdapter(recentChallengeAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.v(TAG, "Challenge item clicked" );
                Intent intent = new Intent(getActivity(),ChallengePageActivity.class);
                Bundle b = new Bundle();
                b.putString("id", recentChallengeAdapter.getItem(position).getID());
                intent.putExtras(b);
                startActivity(intent);
            }
        });

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.add(Menu.NONE, ProjectConstants.MENU_ITEM_CREATE_CHALLENGE, Menu.NONE, "Create Challenge");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case ProjectConstants.MENU_ITEM_CREATE_CHALLENGE:
                // intent CreateChallengeActivity
                Intent intentSet = new Intent(getActivity(), CreateChallengeActivity.class);
                startActivity(intentSet);
                return true;

            default:
                return false;
        }
    }
}
