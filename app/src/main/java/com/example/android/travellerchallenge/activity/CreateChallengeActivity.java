package com.example.android.travellerchallenge.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.travellerchallenge.R;
import com.example.android.travellerchallenge.constant.ProjectConstants;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import com.example.android.travellerchallenge.entity.ChallengeItem;

/**
 * This is the activity to create a Challenge with attributes such name, activities and duration.
 * The challenge can be created from this page by using create menu item button.
 * If the challenge is created, it will appended to the Firebase database's "challenges" child,
 * then the ChallengeID is appended to the "createdChallenges" child of the current user.
 */

public class CreateChallengeActivity extends AppCompatActivity {

    private static final String TAG = "CreateChallengeActivity";

    private Activity context;

    private EditText challengeNameView;
    private NumberPicker dayView, hourView, minuteView;
    private LinearLayout activityLayout;

    private DatabaseReference mDatabase ;

    private ArrayList<String> activities ;
    private ArrayList<Integer> types;

    private float totalDistance;

    private Location location1;
    private Location location2;

    private int score = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_challenge_layout);
        context = this;
        // setting title as "Create Challenge"
        setTitle(ProjectConstants.CREATE_CHALLENGE);

        // initialize base database reference
        mDatabase = FirebaseDatabase.getInstance().getReference();

        // initialize arraylists
        activities = new ArrayList<>();
        types = new ArrayList<>();

        // initialize locations
        location1 = new Location("");
        location2 = new Location("");

        // initialize View items
        challengeNameView = findViewById(R.id.challenge_name);
        dayView = findViewById(R.id.duration_day);
        hourView = findViewById(R.id.duration_hours);
        minuteView = findViewById(R.id.duration_minutes);
        ImageView addButton = findViewById(R.id.add_button);
        activityLayout = findViewById(R.id.activities);

        // setting click listener to addButton which is used to add Activities to the challenge
        // PlacePicker is used to select and add an activity
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addActivity();
            }
        });

        // setting Min and Max values to number pickers which are used to determine the duration of challenge
        dayView.setMaxValue(5);
        dayView.setMinValue(0);
        hourView.setMaxValue(23);
        hourView.setMinValue(0);
        minuteView.setMaxValue(59);
        minuteView.setMinValue(0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        // Adding new menu item
        MenuItem item = menu.add(Menu.NONE, ProjectConstants.MENU_ITEM_CREATE,Menu.NONE,"Done");
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            case ProjectConstants.MENU_ITEM_CREATE:
                // validate then process the click
                if(validation()){
                    onCreateClicked(context);
                }
                // Burada done a basılıyor ancak validation() methoduna bakılmalı.
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // if start place picker is returned with result_ok, get and add place
        if (requestCode == ProjectConstants.PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                // getting place from PlacePicker
                Place place = PlacePicker.getPlace(context,data);
                // create a text view that contains placeName and add to layout
                TextView valueTV = new TextView(context);
                valueTV.setText(String.valueOf(place.getName()));
                valueTV.setPadding(0,0,0,12);
                valueTV.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                activityLayout.addView(valueTV);
                // adding place activity and type info to arraylists
                activities.add(place.getId());
                types.addAll(place.getPlaceTypes());

                if(activities.size() > 1){
                    location2.setLatitude(place.getLatLng().latitude);
                    location2.setLongitude(place.getLatLng().longitude);

                    totalDistance += distanceBetweenTwoLocations(location1,location2);

                    location1.setLongitude(place.getLatLng().longitude);
                    location1.setLatitude(place.getLatLng().latitude);

                    Log.i(TAG, "onActivityResult: updatedTotalDistance: " + totalDistance);
                }
                else{
                    totalDistance = 0;
                    location1.setLongitude(place.getLatLng().longitude);
                    location1.setLatitude(place.getLatLng().latitude);

                    Log.i(TAG, "onActivityResult: updatedTotalDistance: " + totalDistance);
                }

            }
            else{
                Log.i(TAG, "onActivityResult: User clicked change location.");
            }
        }

    }

    /**
     * This method is addActivity method.
     */
    private void addActivity(){
        try {
            // Create place picker
            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            Intent intent = builder.build(context);
            // Start picker for origin place request
            startActivityForResult(intent, ProjectConstants.PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            GooglePlayServicesUtil
                    .getErrorDialog(e.getConnectionStatusCode(), context, 0);
        } catch (GooglePlayServicesNotAvailableException e) {
            Toast.makeText(context, ProjectConstants.GOOGLE_PLAY_SERVICES_NOT_AVAILABLE,
                    Toast.LENGTH_LONG)
                    .show();
        } catch (Exception e){
            Toast.makeText(context, "Error" + e.getMessage(),
                    Toast.LENGTH_LONG)
                    .show();
        }
    }

    /**
     * This method is for calling when create menu button is clicked.
     */
    private void onCreateClicked(final Activity activity) {
        // show alert dialog before proceed
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(ProjectConstants.CREATE_CHALLENGE);
        builder.setMessage(ProjectConstants.CREATE_CHALLENGE_MESSAGE);
        builder.setPositiveButton(ProjectConstants.POSITIVE_ANSWER, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Set score with the magnificent algorithm!
                calculateScore();
                // create challenge
                appendChallenge();
            }
        });
        builder.setNegativeButton(ProjectConstants.NEGATIVE_ANSWER, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) { }
        });
        builder.show();
    }

    /**
     * This method is for appending the challenge to Firebase Database.
     */
    private void appendChallenge(){
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if( user != null) {
            final String creatorID = user.getUid();
            final int duration = durationInMinutes();
            // create a new Challenge Item
            String challengeID = mDatabase.child(ProjectConstants.CHALLENGES_ENTITY).push().getKey();
            ChallengeItem challenge = new ChallengeItem(challengeID, challengeNameView.getText().toString(),
                    creatorID, "", 0.0, duration, score, 0, 0,
                    activities, types, -System.currentTimeMillis());
            // append it to the child with key such challengeID
            mDatabase.child(ProjectConstants.CHALLENGES_ENTITY).child(challengeID).setValue(challenge);
            // append "challengeID" of the created challenge to the challengeCreated child of the current user
            mDatabase.child(ProjectConstants.USERS_ENTITY).child(creatorID).child(ProjectConstants.USERS_CHALLENGES_CREATED).child(challengeID).setValue(true);
            Snackbar.make(findViewById(R.id.linear_layout), ProjectConstants.CHALLENGE_CREATED, Snackbar.LENGTH_LONG).show();
            this.finish();
        }
    }

    /**
     * This method is for calculating duration of the challenge being created in minutes.
     */
    private int durationInMinutes(){
        int days = dayView.getValue();
        int hours = hourView.getValue();
        int minutes = minuteView.getValue();

        return days*1440 + hours*60 + minutes;
    }

    /**
     * This method is to validate the input from user.
     */
    private boolean validation(){

        boolean validInput = true;
        String challengeName = challengeNameView.getText().toString();

        // challenge name must be entered
        if (challengeName.isEmpty()) {
            challengeNameView.setError(ProjectConstants.CHALLENGE_NAME_REQUIRED);
            validInput=false;
        } else {
            challengeNameView.setError(null);
        }

        // At least one activity must be entered
        TextView addActivity = findViewById(R.id.add_activity);
        if(activities.isEmpty()){
            addActivity.setError(ProjectConstants.CHALLENGE_ACTIVITY_REQUIRED);
            validInput=false;
        } else {
            addActivity.setError(null);
        }

        // Duration must be greater than zero minutes
        TextView setDuration = findViewById(R.id.set_duration);
        if(durationInMinutes() == 0){
            setDuration.setError(ProjectConstants.CHALLENGE_DURATION_REQUIRED);
            validInput=false;
        } else {
            setDuration.setError(null);
        }
        return validInput;
    }

    /**
     * This method is for calculating the distance between two locations.
     */
    private float distanceBetweenTwoLocations(Location location1, Location location2){
        return location1.distanceTo(location2);
    }

    /**
     * This method calculates the score of the challenge being created.
     */
    private void calculateScore(){

        float floatScore = ((totalDistance / 1000) / (durationInMinutes() / 60)) * 100 + (activities.size() * 100);

        int notRoundedScore = Math.round(floatScore);

        if(notRoundedScore >= 1000 && notRoundedScore <= 9999){
            score = (notRoundedScore / 100) * 100;
        }
        else if(notRoundedScore >= 10000 && notRoundedScore <= 99999 ){
            score = (notRoundedScore / 1000) * 1000;
        }
        else if(notRoundedScore >= 100000){
            score = 100000;
        }
        else{
            score = (notRoundedScore / 10) * 10;
        }

        Log.i(TAG, "calculateScore: Final calculated score: " + score);
    }

}
