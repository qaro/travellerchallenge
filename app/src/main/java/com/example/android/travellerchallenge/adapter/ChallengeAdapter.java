package com.example.android.travellerchallenge.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.android.travellerchallenge.constant.ProjectConstants;
import com.example.android.travellerchallenge.entity.ChallengeItem;
import com.example.android.travellerchallenge.R;
import com.example.android.travellerchallenge.util.ProjectMethods;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * This class is array adapter class with for Challenge items.
 * That will perform the display of the challenge object attributes.
 */

public class ChallengeAdapter extends ArrayAdapter<ChallengeItem> {

    public ChallengeAdapter(Activity context, ArrayList<ChallengeItem> challenges) {
        super(context, 0, challenges);
    }

    @Override
    @NonNull
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {

        View listItemView = convertView;
        // Check if the existing view is being reused, otherwise inflate the view
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.challenge_item, parent, false);
        }

        final ChallengeItem currentChallenge = getItem(position);
        if(currentChallenge != null){
            // initialize views
            final TextView challengeName = listItemView.findViewById(R.id.challenge_name);
            final TextView city = listItemView.findViewById(R.id.challenge_city);
            final TextView score = listItemView.findViewById(R.id.score);
            final TextView duration = listItemView.findViewById(R.id.duration);
            final TextView rate = listItemView.findViewById(R.id.ratio);
            final TextView reviewCount = listItemView.findViewById(R.id.numOfReviews);
            final TextView creator = listItemView.findViewById(R.id.creator);
            final TextView categories = listItemView.findViewById(R.id.categories);

            // displays attributes
            DatabaseReference mRef = FirebaseDatabase.getInstance().getReference().child(ProjectConstants.USERS_ENTITY).child(currentChallenge.getCreatorID());
            mRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists()){
                        challengeName.setText(currentChallenge.getCName());
                        city.setText(currentChallenge.getCity());
                        score.setText(String.valueOf(currentChallenge.getScore()));
                        duration.setText(ProjectMethods.convertDurationToString((currentChallenge.getDuration())));
                        DecimalFormat df = new DecimalFormat("#.#");
                        rate.setText(String.valueOf(df.format(currentChallenge.getRate())));
                        reviewCount.setText(String.valueOf(currentChallenge.getRateCount()));
                        if (currentChallenge.getTypes() != null) {
                            String c =ProjectMethods.convertTypesToString(currentChallenge.getTypes());
                            categories.setText(c);
                        }
                        creator.setText(dataSnapshot.child(ProjectConstants.USERS_USERNAME).getValue(String.class));
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
        return listItemView;

    }
}
